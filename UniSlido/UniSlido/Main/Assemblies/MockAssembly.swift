//
//  MockAssembly.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation
import Swinject

final class MockAssembly: Assembly {
    // MARK: - Assembly functions

    func assemble(container: Container) {
        container.register(EventDataServiceProtocol.self) { _ in
            MockEventDataService()
        }.inObjectScope(.container)

        container.register(QuestionDataServiceProtocol.self) { _ in
            MockQuestionDataService()
        }.inObjectScope(.container)
    }
}
