//
//  DateTimeUtil.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

class DateTimeUtil {
    public static func timeStampToDateString(timeStamp: Int?) -> String {
        guard let timeStamp = timeStamp else { return "" }

        let date = Date(timeIntervalSince1970: Double(timeStamp))

        return dateToString(date)
    }

    public static func dateToString(_ date: Date?) -> String {
        guard let date = date else { return "" }

        let dateFormatter = DateFormatter()
        let locale = NSLocalizedString("locale_code", comment: "locale key")
        dateFormatter.locale = Locale(identifier: locale)
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short

        return dateFormatter.string(from: date)
    }

    public static func dateToTimeStamp (_ date: Date?) -> Int? {
        guard let date = date else { return nil }

        return Int(date.timeIntervalSince1970)
    }
}
