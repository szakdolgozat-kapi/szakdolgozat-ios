//
//  Injected.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation
import Swinject

@propertyWrapper
struct Injected<Service> {
    private var service: Service?

    var wrappedValue: Service {
        mutating get {
            if service == nil {
                service = assembler.resolver.resolve(Service.self)
            }
            return service!
        }
        mutating set {
            service = newValue
        }
    }
}
