//
//  UIImage+Exts.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

extension UIImage {
    static func imageWithColor(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)

        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        if let context = context {
            context.setFillColor(color.cgColor)
            context.fill(rect)
        }

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
}
