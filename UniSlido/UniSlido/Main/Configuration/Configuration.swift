//
//  Configuration.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation
import Swinject

struct Configuration {
    // MARK: - Types

    struct Endpoint: EndpointProtocol {
        let events = "events"
        let event = "event"
        let lecture = "lecture"
        let canIAsk = "lecture/can_i_ask"
        let ask = "lecture/ask"
        let startLecture = "lecture/register"
        let questions = "lecture/questions"
        let newQuestions = "lecture/new_questions"
        let acceptQuestion = "lecture/accept_question"
        let declineQuestion = "lecture/decline_question"
    }

    // MARK: - Properties

    var apiURL: String {
        switch environment {
        case .debug:
            return "http://127.0.0.1:8080/api/v1/"
        case .release:
            return ""
        }
    }

    var assembly: Assembly {
        switch environment {
        case .debug:
//            return MockAssembly()
            return ReleaseAssembly()
        case .release:
            return ReleaseAssembly()
        }
    }

    let endpoint: EndpointProtocol = Endpoint()
    var isHTTPLoggerEnabled: Bool {
        return CommandLine.arguments.contains("-enable-http-logger")
    }
}

// MARK: - ConfigurationProtocol

extension Configuration: ConfigurationProtocol {}
