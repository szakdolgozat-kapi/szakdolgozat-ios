//
//  EndpointProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol EndpointProtocol {
    var events: String { get }
    var event: String { get }
    var lecture: String { get }
    var canIAsk: String { get }
    var ask: String { get }
    var startLecture: String { get }
    var questions: String { get }
    var newQuestions: String { get }
    var acceptQuestion: String { get }
    var declineQuestion: String { get }
}
