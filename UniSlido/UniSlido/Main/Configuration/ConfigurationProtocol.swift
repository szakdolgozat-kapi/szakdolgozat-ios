//
//  ConfigurationProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation
import Swinject

protocol ConfigurationProtocol {
    var endpoint: EndpointProtocol { get }
    var assembly: Assembly { get }
    var apiURL: String { get }
    var isHTTPLoggerEnabled: Bool { get }
}
