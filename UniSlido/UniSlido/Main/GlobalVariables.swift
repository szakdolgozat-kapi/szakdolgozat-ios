//
//  GlobalVariables.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation
import Swinject

#if DEBUG
let environment = Environment.debug
#elseif RELEASE
let environment = Environment.release
#endif

let config: ConfigurationProtocol = Configuration()
var assembler: Assembler!

enum Segues: String {
    case fromEventsToEventDetails
    case fromEventDetailsToAskAQuestion
    case fromQuestionsToEventDetails
    case fromQuestionsToManageQuestions
    case fromEventsToDateTimeSelector
}
