//
//  Lecture.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct Lecture: Codable {
    var identifier: Int
    var name: String?
    var info: String?
    var eventName: String?
    var placeName: String?
    var startTimeStamp: Int?
    var endTimeStamp: Int?
    var offers: [TeachersOffering]?
    var imageUrl: String?
    var askAQuestionIdentifier: String?
}
