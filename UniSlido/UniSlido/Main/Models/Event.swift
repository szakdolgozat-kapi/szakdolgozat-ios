//
//  Event.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct Event: Codable, Equatable {
    // MARK: - Properties

    var identifier: Int
    var name: String?
    var info: String?
    var startTimeStamp: Int?
    var endTimeStamp: Int?

    var lectures: [Lecture] = []

    // MARK: - Equatable functions

    static func == (lhs: Event, rhs: Event) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
