//
//  Question.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct Question: Codable {
    var identifier: Int
    var questionString: String?
    var lectureId: String
}
