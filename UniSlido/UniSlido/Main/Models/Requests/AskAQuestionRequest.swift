//
//  AskAQuestionRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct AskAQuestionRequest: RequestProtocol {
    let endpoint = config.endpoint.ask
    let method = RequestMethod.post

    let lectureId: Int
    let question: String

    var queryParameters: [String: Any]? {
        ["lectureId": lectureId,
         "question": question]
    }

    init(lectureId: Int, question: String) {
        self.lectureId = lectureId
        self.question = question
    }
}
