//
//  EventsRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 31..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct EventsRequest: RequestProtocol {
    let endpoint = config.endpoint.events
    let method = RequestMethod.get
}
