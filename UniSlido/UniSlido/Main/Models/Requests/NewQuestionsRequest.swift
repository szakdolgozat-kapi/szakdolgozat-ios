//
//  NewQuestionsRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct NewQuestionsRequest: RequestProtocol {
    let endpoint = config.endpoint.newQuestions
    let method = RequestMethod.get

    let lectureId: String
    let lastQuestionId: Int?

    var queryParameters: [String: Any]? {
        let lastQuestionId: String
        if let lastId = self.lastQuestionId {
            lastQuestionId = lastId.description
        } else {
            lastQuestionId = ""
        }

        return ["lectureId": lectureId,
                "lastQuestionId": lastQuestionId]
    }

    init(lectureId: String, lastQuestionId: Int?) {
        self.lectureId = lectureId
        self.lastQuestionId = lastQuestionId
    }
}
