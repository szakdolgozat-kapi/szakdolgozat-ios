//
//  CanIAskRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct CanIAskRequest: RequestProtocol {
    let endpoint = config.endpoint.canIAsk
    let method = RequestMethod.get

    let lectureId: Int

    var queryParameters: [String : Any]? {
        ["lectureId": lectureId]
    }

    init(lectureId: Int) {
        self.lectureId = lectureId
    }
}
