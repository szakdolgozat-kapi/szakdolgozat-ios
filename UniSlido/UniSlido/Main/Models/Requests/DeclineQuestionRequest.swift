//
//  DeclineQuestionRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct DeclineQuestionRequest: RequestProtocol {
    let endpoint = config.endpoint.declineQuestion
    let method = RequestMethod.post

    let lectureId: String
    let questionId: Int

    var queryParameters: [String: Any]? {
        ["lectureId": lectureId,
         "questionId": questionId]
    }

    init(lectureId: String, questionId: Int) {
        self.lectureId = lectureId
        self.questionId = questionId
    }
}
