//
//  QuestionsRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct QuestionsRequest: RequestProtocol {
    let endpoint = config.endpoint.questions
    let method = RequestMethod.get

    let lectureId: String

    var queryParameters: [String: Any]? {
        ["lectureId": lectureId]
    }

    init(lectureId: String) {
        self.lectureId = lectureId
    }
}
