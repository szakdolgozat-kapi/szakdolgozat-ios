//
//  StartLectureRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct StartLectureRequest: RequestProtocol {
    let endpoint = config.endpoint.startLecture
    let method = RequestMethod.post

    let teacherCode: String

    var queryParameters: [String: Any]? {
        ["teacherCode": teacherCode]
    }

    init(teacherCode: String) {
        self.teacherCode = teacherCode
    }
}
