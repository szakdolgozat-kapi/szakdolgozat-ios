//
//  LectureRequest.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct LectureRequest: RequestProtocol {
    let endpoint = config.endpoint.lecture
    let method = RequestMethod.get

    let lectureId: Int

    var queryParameters: [String: Any]? {
        ["lectureId": lectureId]
    }

    init(lectureId: Int) {
        self.lectureId = lectureId
    }
}
