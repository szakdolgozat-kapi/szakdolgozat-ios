//
//  EventFilter.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol EventFilterDataSource: class {
    var events: [Event]? { get }
    var filteredEvents: [Event] { get }
}

protocol EventFilterDelegate: class {
    func nameChanged(_ name: String?)
    func startDateChanged(_ startDate: Date?)
    func endDateChanged(_ endDate: Date?)
    func teachersOfferingChanged(_ on: Bool)
    func filteredListChanged(filteredEvents: [Event])
}

extension EventFilterDelegate {
    func nameChanged(_ name: String?) {}
    func startDateChanged(_ startDate: Date?) {}
    func endDateChanged(_ endDate: Date?) {}
    func teachersOfferingChanged(_ on: Bool) {}
}

class EventFilter {
    // MARK: - Properties

    weak var delegate: EventFilterDelegate?
    weak var dataSource: EventFilterDataSource?

    var name: String? {
        didSet {
            delegate?.nameChanged(name)
            filter()
        }
    }
    var startDate: Date? {
        didSet {
            delegate?.startDateChanged(startDate)
            filter()
        }
    }
    var endDate: Date? {
        didSet {
            delegate?.endDateChanged(endDate)
            filter()
        }
    }
    var teachersOffering: Bool = false {
        didSet {
            delegate?.teachersOfferingChanged(teachersOffering)
            filter()
        }
    }

    // MARK: - Functions

    func filter() {
        guard let events = dataSource?.events, !events.isEmpty else {
            delegate?.filteredListChanged(filteredEvents: [])
            return
        }

        var filteredArray = events

        // search in lecture's name and info
        var tmp = Array(filteredArray)

        filteredArray.forEach({ event in
            if let name = name, !name.isEmpty {
                let filteredLectures = event.lectures.filter({
                    ($0.name != nil &&
                        $0.name!.lowercased().contains(name.lowercased())) ||
                        $0.info != nil &&
                        $0.info!.lowercased().contains(name.lowercased())
                })

                let index = tmp.firstIndex(of: event)
                if filteredLectures.count == 0 {
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = filteredLectures
                }
            }
        })
        filteredArray = Array(tmp)

        // filter for lecture's start date
        tmp = Array(filteredArray)

        filteredArray.forEach({ event in
            if let startDate = startDate {
                let filteredLectures = event.lectures.filter({
                    $0.startTimeStamp != nil &&
                        $0.startTimeStamp! >= DateTimeUtil.dateToTimeStamp(startDate) ?? 0
                })

                let index = tmp.firstIndex(of: event)
                if filteredLectures.count == 0 {
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = filteredLectures
                }
            }
        })
        filteredArray = Array(tmp)

        // filter for lecture's end date
        tmp = Array(filteredArray)

        filteredArray.forEach({ event in
            if let endDate = endDate {
                let filteredLectures = event.lectures.filter({
                    $0.endTimeStamp != nil &&
                        $0.endTimeStamp! >= DateTimeUtil.dateToTimeStamp(endDate) ?? 0
                })

                let index = tmp.firstIndex(of: event)
                if filteredLectures.count == 0 {
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = filteredLectures
                }
            }
        })
        filteredArray = Array(tmp)

        // filter for lecture's teachers offering
        tmp = Array(filteredArray)

        filteredArray.forEach({ event in
            if teachersOffering {
                let filteredLectures = event.lectures.filter({
                    $0.offers != nil &&
                        !($0.offers?.isEmpty ?? true)
                })

                let index = tmp.firstIndex(of: event)
                if filteredLectures.count == 0 {
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = filteredLectures
                }
            }
        })
        filteredArray = Array(tmp)

        delegate?.filteredListChanged(filteredEvents: filteredArray)
    }
}
