//
//  StartLectureResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct StartLectureResponse: BaseResponse {
    var status: Status
    var data: StartLectureWrapper?

    var lecture: Lecture? {
        data?.lecturePersistence
    }
}

struct StartLectureWrapper: Codable {
    var lecturePersistence: Lecture?
}
