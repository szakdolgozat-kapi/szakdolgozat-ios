//
//  NewQuestionsResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct NewQuestionsResponse: BaseResponse {
    var status: Status
    var data: NewQuestionsWrapper?

    var questions: [Question]? {
        data?.arrayList
    }
}

struct NewQuestionsWrapper: Codable {
    var arrayList: [Question]?
}
