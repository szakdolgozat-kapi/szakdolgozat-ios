//
//  AskAQuestionResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct AskAQuestionResponse: BaseResponse {
    var status: Status
    var data: AskAQuestionWrapper?

    var accepted: Bool? {
        data?.boolean
    }
}

struct AskAQuestionWrapper: Codable {
    var boolean: Bool?
}
