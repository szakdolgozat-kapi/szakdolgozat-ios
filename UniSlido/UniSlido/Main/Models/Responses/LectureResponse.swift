//
//  LectureResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct LectureResponse: BaseResponse {
    var status: Status
    var data: LectureWrapper?

    var lecture: Lecture? {
        data?.lecturePersistence
    }
}

struct LectureWrapper: Codable {
    var lecturePersistence: Lecture?
}
