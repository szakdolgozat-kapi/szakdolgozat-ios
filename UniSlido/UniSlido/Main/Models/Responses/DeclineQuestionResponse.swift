//
//  DeclineQuestionResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct DeclineQuestionResponse: BaseResponse {
    var status: Status
    var data: DeclineQuestionWrapper?

    var accepted: Bool? {
        data?.boolean
    }
}

struct DeclineQuestionWrapper: Codable {
    var boolean: Bool?
}
