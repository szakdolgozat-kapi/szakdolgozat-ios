//
//  AcceptQuestionResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct AcceptQuestionResponse: BaseResponse {
    var status: Status
    var data: AcceptQuestionWrapper?

    var accepted: Bool? {
        data?.boolean
    }
}

struct AcceptQuestionWrapper: Codable {
    var boolean: Bool?
}
