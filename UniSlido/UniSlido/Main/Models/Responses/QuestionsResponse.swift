//
//  QuestionsResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct QuestionsResponse: BaseResponse {
    var status: Status
    var data: QuestionsWrapper?

    var questions: [Question]? {
        data?.treeSet
    }
}

struct QuestionsWrapper: Codable {
    var treeSet: [Question]?
}
