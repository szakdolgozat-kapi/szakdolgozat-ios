//
//  CanIAskResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 11. 03..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct CanIAskResponse: BaseResponse {
    var status: Status
    var data: CanIAskWrapper?

    var lecture: Lecture? {
        data?.lecturePersistence
    }

    var accepted: Bool? {
        data?.boolean
    }
}

struct CanIAskWrapper: Codable {
    var lecturePersistence: Lecture?
    var boolean: Bool?
}
