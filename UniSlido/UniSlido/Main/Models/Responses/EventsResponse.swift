//
//  EventsResponse.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct EventsResponse: BaseResponse {
    var status: Status
    var data: EventWrapper?

    var events: [Event]? {
        data?.arrayList
    }
}

struct EventWrapper: Codable {
    var arrayList: [Event]?
}
