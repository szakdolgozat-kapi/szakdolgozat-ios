//
//  Status.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

struct Status: Codable {
    var status: Int
    var messageEn: String
    var messageHu: String

    var localizedMessage: String {
        let localeCode = NSLocalizedString("locale_code", comment: "")
        if localeCode == "hu_HU" {
            return messageHu
        } else {
            return messageEn
        }
    }
}
