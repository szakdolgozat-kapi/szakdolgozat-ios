//
//  RequestMethod.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

enum RequestMethod: String {
    case head = "HEAD"
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
}
