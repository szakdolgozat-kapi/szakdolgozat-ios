//
//  NetworkServiceProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    func requestObject<T: Codable>(_ request: RequestProtocol,
                                   type: T.Type,
                                   success: ((T) -> Void)?,
                                   failure: ((Error) -> Void)?)
}
