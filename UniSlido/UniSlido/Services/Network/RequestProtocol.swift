//
//  RequestProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol RequestProtocol {
    var endpoint: String { get }
    var fullURL: URL? { get }
    var method: RequestMethod { get }
    var header: [String: String] { get }

    /// If you create a GET or a DELETE request, please use ONLY query params (bodyParams will be ignored)
    var queryParameters: [String: Any]? { get }
    var bodyParameters: [String: Any]? { get }
}

extension RequestProtocol {
    public var queryParameters: [String: Any]? {
        return nil
    }

    public var bodyParameters: [String: Any]? {
        return nil
    }

    var fullURL: URL? {
        guard let url = URL(string: config.apiURL + endpoint),
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
                return nil
        }

        if let queryItems = NetworkQueryGeneretor.makeQueryItems(params: queryParameters) {
            if components.queryItems != nil {
                components.queryItems?.append(contentsOf: queryItems)
            } else {
                components.queryItems = queryItems
            }
        }

        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "@", with: "%40")

        return components.url
    }

    var header: [String: String] {
        return defaultHeader()
    }

    func defaultHeader() -> [String: String] {
        return ["Accept": "application/json",
                "UUID": UUID().uuidString]
    }

    func createURLRequest() throws -> URLRequest {
        guard let url = fullURL else {
            throw NetworkServiceError.invalidUrl
        }

        var request = URLRequest(url: url,
                                 cachePolicy: .useProtocolCachePolicy,
                                 timeoutInterval: 30)
        request.httpMethod = request.httpMethod
        header.forEach({ request.addValue($0.value, forHTTPHeaderField: $0.key) })

        if method != .get, method != .delete, let params = bodyParameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }

        return request
    }
}
