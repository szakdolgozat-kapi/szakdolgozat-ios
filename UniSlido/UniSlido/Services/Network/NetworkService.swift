//
//  NetworkService.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

// MARK: - Types

enum NetworkServiceError: Error {
    case invalidUrl
    case invalidData
    case cannotParse
    case serverError(Status)
}

class NetworkService {
    // MARK: - Properties

    private lazy var decoder: JSONDecoder = {
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }()

    // MARK: - Functions

    private func parseObject<T: Codable>(type: T.Type, data: Data?) throws -> T {
        guard let data = data else {
            throw NetworkServiceError.invalidData
        }

        do {
            return try decoder.decode(T.self, from: data)
        } catch let error {
            debugPrint("Error while parsing: \(error)")
            throw NetworkServiceError.cannotParse
        }
    }

    private func logResponse(networkResponse: (data: Data?, urlResponse: URLResponse?, error: Error?)) {
        guard config.isHTTPLoggerEnabled else {
            return
        }

        if let error = networkResponse.error {
            HTTPLogger.logError(error as NSError)
        } else {
            HTTPLogger.logResponse(networkResponse.urlResponse, data: networkResponse.data)
        }
    }
}

// MARK: - NetworkServiceProtocol

extension NetworkService: NetworkServiceProtocol {
    func requestObject<T: Codable>(_ request: RequestProtocol,
                                   type: T.Type,
                                   success: ((T) -> Void)?,
                                   failure: ((Error) -> Void)?) {
        let urlRequest: URLRequest
        do {
            urlRequest = try request.createURLRequest()
        } catch {
            failure?(error)
            return
        }

        if config.isHTTPLoggerEnabled {
            HTTPLogger.logRequest(urlRequest)
        }

        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { [weak self] data, urlResponse, error in
            guard let self = self else { return }

            self.logResponse(networkResponse: (data, urlResponse, error))
            do {
                let response = try self.parseObject(type: type, data: data)
                try self.checkStatus(response: response)
                DispatchQueue.main.async {
                    success?(response)
                }
            } catch {
                DispatchQueue.main.async {
                    failure?(error)
                }
            }
        })
        task.resume()
    }

    // MARK: - Functions

    private func checkStatus(response: Any) throws {
        guard let response = response as? BaseResponse else {
            throw NetworkServiceError.cannotParse
        }

        if response.status.status != 200 {
            throw NetworkServiceError.serverError(response.status)
        }
    }
}
