//
//  QuestionDataServiceProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol QuestionDataServiceProtocol {
    func lecture(by id: Int, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?)
    func canIAsk(for lectureId: Int, success: ((Bool, Lecture) -> Void)?, failure: ((Error) -> Void)?)
    func askAQuestion(questionString: String, lectureId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?)
    func startLecture(with id: String, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?)
    func questions(for lectureId: String, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?)
    func newQuestions(for lectureId: String, lastQuestionId: Int?, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?)
    func acceptQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?)
    func declineQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?)
}
