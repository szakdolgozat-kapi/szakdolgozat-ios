//
//  QuestionDataService.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

class QuestionDataService: QuestionDataServiceProtocol {
    // MARK: - Properties

    @Injected private var networkService: NetworkServiceProtocol

    // MARK: - QuestionDataServiceProtocol functions

    func lecture(by id: Int, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            LectureRequest(lectureId: id),
            type: LectureResponse.self,
            success: { response in
                guard let lecture = response.lecture else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(lecture)
            },
            failure: failure
        )
    }

    func canIAsk(for lectureId: Int, success: ((Bool, Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            CanIAskRequest(lectureId: lectureId),
            type: CanIAskResponse.self,
            success: { response in
                guard let accepted = response.accepted,
                    let lecture = response.lecture else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(accepted, lecture)
            },
            failure: failure
        )
    }

    func askAQuestion(questionString: String, lectureId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            AskAQuestionRequest(lectureId: lectureId, question: questionString),
            type: AskAQuestionResponse.self,
            success: { response in
                guard let accepted = response.accepted else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(accepted)
            },
            failure: failure
        )
    }

    func startLecture(with id: String, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            StartLectureRequest(teacherCode: id),
            type: StartLectureResponse.self,
            success: { response in
                guard let lecture = response.lecture else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(lecture)
            },
            failure: failure
        )
    }

    func questions(for lectureId: String, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            QuestionsRequest(lectureId: lectureId),
            type: QuestionsResponse.self,
            success: { response in
                guard let questions = response.questions else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(questions)
            },
            failure: failure
        )
    }

    func newQuestions(for lectureId: String, lastQuestionId: Int?, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            NewQuestionsRequest(lectureId: lectureId, lastQuestionId: lastQuestionId),
            type: NewQuestionsResponse.self,
            success: { response in
                guard let questions = response.questions else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(questions)
            },
            failure: failure
        )
    }

    func acceptQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            AcceptQuestionRequest(lectureId: lectureId, questionId: questionId),
            type: AcceptQuestionResponse.self,
            success: { response in
                guard let accepted = response.accepted else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(accepted)
            },
            failure: failure
        )
    }

    func declineQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            DeclineQuestionRequest(lectureId: lectureId, questionId: questionId),
            type: DeclineQuestionResponse.self,
            success: { response in
                guard let accepted = response.accepted else {
                    failure?(NetworkServiceError.cannotParse)
                    return
                }

                success?(accepted)
            },
            failure: failure
        )
    }
}
