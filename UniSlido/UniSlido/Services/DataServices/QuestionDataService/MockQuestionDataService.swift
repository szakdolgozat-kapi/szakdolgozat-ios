//
//  MockQuestionDataService.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

class MockQuestionDataService: QuestionDataServiceProtocol {
    // MARK: - QuestionDataServiceProtocol functions

    func lecture(by id: Int, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(self.fakeLecture())
        }
    }

    func canIAsk(for lectureId: Int, success: ((Bool, Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(true, self.fakeLecture())
        }
    }

    func askAQuestion(questionString: String, lectureId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(true)
        }
    }

    func startLecture(with id: String, success: ((Lecture) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(self.fakeLecture())
        }
    }

    func questions(for lectureId: String, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(self.fakeQuestions())
        }
    }

    func newQuestions(for lectureId: String, lastQuestionId: Int?, success: (([Question]) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(self.fakeQuestions())
        }
    }

    func acceptQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(true)
        }
    }

    func declineQuestion(for lectureId: String, questionId: Int, success: ((Bool) -> Void)?, failure: ((Error) -> Void)?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            success?(true)
        }
    }

    // MARK: - Functions

    // swiftlint:disable line_length identifier_name
    private func fakeLecture() -> Lecture {
        let lectureName = "Lorem ipsum dolor sit amet: 0-0"
        let lectureInfo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        let lectureStartTimeStamp = 1484347957 + 3600
        let lectureEndTimeStamp = lectureStartTimeStamp + 5400

        let o0 = TeachersOffering(teacher: "teacher 0-0", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        let o1 = TeachersOffering(teacher: "teacher 0-1", course: "course", offer: "offer")
        let o2 = TeachersOffering(teacher: "teacher 0-2", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        let o3 = TeachersOffering(teacher: "teacher 0-3", course: "course", offer: "offer")

        let lectureEventName = "Event 0"
        let lecturePlaceName = "IK-F01"
        let lectureAskAQuestionIdentifier = "0001"
        let lectureImageUrl = "https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg"

        let lecture = Lecture(identifier: 0,
        name: lectureName,
        info: lectureInfo,
        eventName: lectureEventName,
        placeName: lecturePlaceName,
        startTimeStamp: lectureStartTimeStamp,
        endTimeStamp: lectureEndTimeStamp,
        offers: [o0, o1, o2, o3],
        imageUrl: lectureImageUrl,
        askAQuestionIdentifier: lectureAskAQuestionIdentifier)

        return lecture
    }

    private func fakeQuestions() -> [Question] {
        var questionArray = [Question]()
        let q0 = Question(identifier: 0, questionString: "Mik a Kanban előnyei Scrummal szemben?", lectureId: "0001")
        questionArray.append(q0)

        let q1 = Question(identifier: 1, questionString: "question 1", lectureId: "0001")
        questionArray.append(q1)

        let q2 = Question(identifier: 2, questionString: "question 2", lectureId: "0001")
        questionArray.append(q2)

        return questionArray
    }
}
