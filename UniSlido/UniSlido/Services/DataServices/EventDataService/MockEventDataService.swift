//
//  MockEventDataService.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

class MockEventDataService: EventDataServiceProtocol {
    // MARK: - EventDataServiceProtocol functions

    func fetchEvents(success: (([Event]) -> Void)?, failure: ((Error) -> Void)?) {
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            success?(MockEventDataService.mockEventList())
        }
    }

    func fetchEvent(by id: String, success: ((Event) -> Void)?, failure: ((Error) -> Void)?) {
    }

    // MARK: - Functions

    // swiftlint:disable line_length identifier_name
    private static func mockEventList() -> [Event] {
        var array = [Event]()
        var counter: Int = 0

        for i in 0...2 {
            let eventName = "Event " + String(i)
            var lectures = [Lecture]()

            for j in 0...4 {
                let lectureImageUrl: String?
                switch j {
                case 0:
                    lectureImageUrl = nil
                case 1:
                    lectureImageUrl = "https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg"
                case 2:
                    lectureImageUrl = "http://s3.amazonaws.com/libapps/accounts/27060/images/example.png"
                case 3:
                    lectureImageUrl = nil
                case 4:
                    lectureImageUrl = "https://fanart.tv/fanart/music/ddeb3502-8693-4619-b41d-263105f84477/musiclogo/example-4e0660c8549ac.png"
                case 5:
                    lectureImageUrl = "http://image.shutterstock.com/z/stock-photo-sample-stamp-showing-example-symbol-or-taste-104401619.jpg"
                default:
                    lectureImageUrl = nil
                }

                let lectureName = "Lorem ipsum dolor sit amet: " + String(i) + "-" + String(j)
                let lectureInfo = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                let lectureStartTimeStamp = 1484347957 + (counter * 3600)
                let lectureEndTimeStamp = lectureStartTimeStamp + 5400

                let o0 = TeachersOffering(teacher: "teacher " + String(counter) + " -0", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                let o1 = TeachersOffering(teacher: "teacher " + String(counter) + " -1", course: "course", offer: "offer")
                let o2 = TeachersOffering(teacher: "teacher " + String(counter) + " -2", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                let o3 = TeachersOffering(teacher: "teacher " + String(counter) + " -3", course: "course", offer: "offer")

                let lectureEventName = eventName
                let lecturePlaceName = "IK-F01"
                let lectureAskAQuestionIdentifier = counter % 2 == 0 ? nil : "000" + String(counter)

                let lecture = Lecture(identifier: counter,
                name: lectureName,
                info: lectureInfo,
                eventName: lectureEventName,
                placeName: lecturePlaceName,
                startTimeStamp: lectureStartTimeStamp,
                endTimeStamp: lectureEndTimeStamp,
                offers: [o0, o1, o2, o3],
                imageUrl: lectureImageUrl,
                askAQuestionIdentifier: lectureAskAQuestionIdentifier)

                counter += 1

                lectures.append(lecture)
            }

            let event = Event(identifier: i,
            name: "Event " + String(i),
            info: nil,
            startTimeStamp: nil,
            endTimeStamp: nil,
            lectures: lectures)

            array.append(event)
        }

        return array
    }
}
