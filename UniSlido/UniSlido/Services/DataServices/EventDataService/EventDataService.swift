//
//  EventDataService.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

class EventDataService: EventDataServiceProtocol {
    // MARK: - Properties

    @Injected private var networkService: NetworkServiceProtocol

    // MARK: - EventDataServiceProtocol functions

    func fetchEvents(success: (([Event]) -> Void)?, failure: ((Error) -> Void)?) {
        networkService.requestObject(
            EventsRequest(),
            type: EventsResponse.self,
            success: { response in
                success?(response.events ?? [])
            },
            failure: failure
        )
    }
}
