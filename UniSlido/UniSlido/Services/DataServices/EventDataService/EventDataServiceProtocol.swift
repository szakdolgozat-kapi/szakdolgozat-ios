//
//  EventDataServiceProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol EventDataServiceProtocol {
    func fetchEvents(success: (([Event]) -> Void)?, failure: ((Error) -> Void)?)
}
