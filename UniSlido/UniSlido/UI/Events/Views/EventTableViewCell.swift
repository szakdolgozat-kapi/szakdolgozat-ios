//
//  EventTableViewCell.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Nuke
import UIKit

class EventTableViewCell: UITableViewCell {
    public static let reuseIdentifier = "EventTableViewCell"

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var cellImageView: UIImageView!

    public func configureCell(lecture: Lecture) {
        if let url = URL(string: lecture.imageUrl ?? "") {
            let options = ImageLoadingOptions(
                transition: .fadeIn(duration: 0.33),
                failureImage: #imageLiteral(resourceName: "ik_logo")
            )
            Nuke.loadImage(with: url, options: options, into: cellImageView)
        }

        nameLabel.text = lecture.name
        infoLabel.text = lecture.info

        if lecture.startTimeStamp != nil && lecture.startTimeStamp! > 0 {
            dateLabel.text = DateTimeUtil.timeStampToDateString(timeStamp: lecture.startTimeStamp)

            if lecture.endTimeStamp != nil && lecture.endTimeStamp! > 0 {
                let tmp = dateLabel.text! + " - " + DateTimeUtil.timeStampToDateString(timeStamp: lecture.endTimeStamp)
                dateLabel.text = tmp
            }
        } else {
            dateLabel.text = ""
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        cellImageView?.image = #imageLiteral(resourceName: "ik_logo")
    }
}
