//
//  OfferTableViewCell.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    public static let reuseIdentifier = "OfferTableViewCell"

    @IBOutlet private weak var teacherNameLabel: UILabel!
    @IBOutlet private weak var courseNameLabel: UILabel!
    @IBOutlet private weak var offerLabel: UILabel!

    func configureCell(offer: TeachersOffering) {
        teacherNameLabel.text = offer.teacher
        courseNameLabel.text = offer.course
        offerLabel.text = offer.offer
    }
}
