//
//  EventsViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit
import Lottie
import Swinject

final class EventsViewController: BaseViewController {
    // MARK: - Types

    private struct PrivateConstants {
        static let filterBoxOpenHeight: CGFloat = 182
        static let filterBoxClosedHeight: CGFloat = 57
        static let animationDuration: TimeInterval = 0.3
    }

    // MARK: - Properties

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var filterContainerView: UIView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var filterButton: UIButton!
    @IBOutlet private weak var filterContainerViewHeight: NSLayoutConstraint!

    @IBOutlet private weak var startDateTitleLabel: UILabel!
    @IBOutlet private weak var startDateLabel: ClearableLabel!

    @IBOutlet private weak var endDateTitleLabel: UILabel!
    @IBOutlet private weak var endDateLabel: ClearableLabel!

    @IBOutlet private weak var teachersOfferingTitleLabel: UILabel!
    @IBOutlet private weak var teachersOfferingSwitch: UISwitch!

    @IBOutlet private weak var noElementsContainer: UIView!
    @IBOutlet private weak var noElementsLabel: UILabel!

    @IBOutlet private weak var loaderContainer: UIView!
    @IBOutlet private weak var loaderAnimationView: AnimationView!

    private lazy var eventFilter: EventFilter = {
        var eventFilter = EventFilter()
        eventFilter.delegate = self
        eventFilter.dataSource = self

        return eventFilter
    }()

    var events: [Event]? {
        didSet {
            filteredEvents = events ?? []
        }
    }

    var filteredEvents: [Event] = [] {
        didSet {
            tableView.reloadData()
            presentEmpty(filteredEvents.isEmpty)
        }
    }

    @Injected private var eventDataService: EventDataServiceProtocol!

    private lazy var picker: DateTimePicker = {
        let picker = DateTimePicker()
        picker.delegate = self

        return picker
    }()

    private var isFilterBoxOpen = false {
        didSet {
            view.endEditing(true)
            picker.dismissViewWithoutCompletion()
            if isFilterBoxOpen {
                filterContainerViewHeight.constant = PrivateConstants.filterBoxOpenHeight
            } else {
                filterContainerViewHeight.constant = PrivateConstants.filterBoxClosedHeight
            }
            UIView.animate(withDuration: PrivateConstants.animationDuration) {
                self.view.layoutIfNeeded()
            }
        }
    }

    // MARK: - BaseViewController functions

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchEvents()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.fromEventsToEventDetails.rawValue,
            let lecture = sender as? Lecture,
            let destinationVC = segue.destination as? EventDetailsViewController {
            destinationVC.lecture = lecture
        }
        super.prepare(for: segue, sender: sender)
    }

    // MARK: - BaseViewControllerProtocol functions

    override func setupView() {
        tableView.tableFooterView = UIView()

        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.layer.borderColor = UIColor.separator.cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = 8

        startDateLabel.delegate = self
        startDateLabel.layer.borderColor = UIColor.separator.cgColor
        startDateLabel.layer.borderWidth = 1
        startDateLabel.layer.cornerRadius = 8
        startDateLabel.clipsToBounds = true

        endDateLabel.delegate = self
        endDateLabel.layer.borderColor = UIColor.separator.cgColor
        endDateLabel.layer.borderWidth = 1
        endDateLabel.layer.cornerRadius = 8
        endDateLabel.clipsToBounds = true

        filterButton.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
        teachersOfferingSwitch.addTarget(self, action: #selector(teachersOfferingSwitched), for: .valueChanged)
    }

    override func translateView() {
        startDateTitleLabel.text = NSLocalizedString("events.start.date.title", comment: "")
        endDateTitleLabel.text = NSLocalizedString("events.end.date.title", comment: "")
        teachersOfferingTitleLabel.text = NSLocalizedString("events.teachers.offering.title", comment: "")
        noElementsLabel.text = NSLocalizedString("events.filter.no.results", comment: "")
    }

    // MARK: - Functions

    @objc
    private func filterButtonTapped() {
        isFilterBoxOpen.toggle()
    }

    @objc
    private func teachersOfferingSwitched() {
        eventFilter.teachersOffering = teachersOfferingSwitch.isOn
    }

    private func fetchEvents() {
        showLoading(true)
        eventDataService.fetchEvents(
            success: { [weak self] events in
                guard let self = self else { return }

                self.showLoading(false)
                self.events = events
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showLoading(false)
                self.showNetworkErrorAlert(error: error, retryBlock: self.fetchEvents)
            }
        )
    }

    private func showLoading(_ show: Bool) {
        guard loaderContainer.isHidden == show else {
            return
        }

        loaderContainer.isHidden = !show
        filterContainerView.isUserInteractionEnabled = !show

        if show {
            loaderAnimationView.play()
        } else {
            loaderAnimationView.stop()
        }
    }

    private func presentEmpty(_ show: Bool) {
        noElementsContainer.isHidden = !show
    }
}

// MARK: - UITableViewDataSource

extension EventsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredEvents.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredEvents[safe: section]?.lectures.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let lecture = filteredEvents[safe: indexPath.section]?.lectures[safe: indexPath.row] else {
            fatalError("Invalid indexPath")
        }

        let cell: EventTableViewCell = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.reuseIdentifier, for: indexPath) as! EventTableViewCell
        cell.configureCell(lecture: lecture)

        return cell
    }
}

// MARK: - UITableViewDelegate

extension EventsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        isFilterBoxOpen = false

        guard let lecture = filteredEvents[safe: indexPath.section]?.lectures[safe: indexPath.row] else {
            fatalError("Invalid indexPath")
        }

        performSegue(withIdentifier: Segues.fromEventsToEventDetails.rawValue, sender: lecture)
    }
}

// NOTE: - ClearableLabelDelegate

extension EventsViewController: ClearableLabelDelegate {
    func labelTextDidCleared(label: ClearableLabel) {
        if label == startDateLabel {
            eventFilter.startDate = nil
        } else if label == endDateLabel {
            eventFilter.endDate = nil
        }
    }

    func labelTapped(label: ClearableLabel) {
        var selectedDate: Date?
        var completionHandler: ((Date) -> Void )?
        if label == startDateLabel {
            startDateLabel.backgroundColor = .systemFill
            endDateLabel.backgroundColor = .clear
            selectedDate = eventFilter.startDate
            completionHandler = { [weak self] date in
                self?.eventFilter.startDate = date
            }
        } else if label == endDateLabel {
            endDateLabel.backgroundColor = .systemFill
            startDateLabel.backgroundColor = .clear
            selectedDate = eventFilter.endDate
            completionHandler = { [weak self] date in
                self?.eventFilter.endDate = date
            }
        }
        picker.completionHandler = completionHandler
        picker.show(selected: selectedDate)
    }
}

// MARK: - EventFilterDelegate

extension EventsViewController: EventFilterDelegate {
    func startDateChanged(_ startDate: Date?) {
        startDateLabel.text = DateTimeUtil.dateToString(startDate)
    }

    func endDateChanged(_ endDate: Date?) {
        endDateLabel.text = DateTimeUtil.dateToString(endDate)
    }

    func filteredListChanged(filteredEvents: [Event]) {
        self.filteredEvents = filteredEvents
    }
}

// MARK: - EventFilterDataSource

extension EventsViewController: EventFilterDataSource {}

// MARK: - UISearchBarDelegate

extension EventsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        eventFilter.name = searchText
    }
}

extension EventsViewController: DateTimePickerDelegate {
    func dateTimePickerDidDismiss(picker: DateTimePicker) {
        self.endDateLabel.backgroundColor = UIColor.clear
        self.startDateLabel.backgroundColor = UIColor.clear
    }
}
