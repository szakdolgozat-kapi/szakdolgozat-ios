//
//  EventDetailsViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import Nuke
import UIKit

final class EventDetailsViewController: BaseViewController {
    // MARK: - BaseViewControllerProtocol properties

    override var isNavigationBarHidden: Bool {
        return false
    }

    // MARK: - Properties

    @IBOutlet private weak var eventNameLabel: UILabel?
    @IBOutlet private weak var placeLabel: UILabel?
    @IBOutlet private weak var descriptionTextView: CollapsableTextView?
    @IBOutlet private weak var questionButton: Button?
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var tableView: UITableView!

    var lecture: Lecture?
    @Injected private var questionDataService: QuestionDataServiceProtocol!

    // MARK: - BaseViewControllerProtocol functions

    override func setupView() {
        super.setupView()

        if let eventName = lecture?.eventName, !eventName.isEmpty {
            eventNameLabel?.text = eventName
        } else {
            eventNameLabel?.removeFromSuperview()
        }

        if let placeName = lecture?.placeName, !placeName.isEmpty {
            placeLabel?.text = placeName
        } else {
            placeLabel?.removeFromSuperview()
        }

        if let info = lecture?.info, !info.isEmpty {
            descriptionTextView?.text = info
        } else {
            descriptionTextView?.removeFromSuperview()
        }

        if lecture?.askAQuestionIdentifier?.isEmpty ?? true {
            questionButton?.removeFromSuperview()
        }

        if let imageURL = URL(string: lecture?.imageUrl ?? "") {
            let options = ImageLoadingOptions(
                transition: .fadeIn(duration: 0.33),
                failureImage: #imageLiteral(resourceName: "ik_logo")
            )
            Nuke.loadImage(with: imageURL, options: options, into: imageView)
        }

        if let offers = lecture?.offers, !offers.isEmpty {
            tableView.reloadData()
        }
        tableView.tableFooterView = UIView()
    }

    override func translateView() {
        title = NSLocalizedString("event.details.title", comment: "")
        questionButton?.setTitle(NSLocalizedString("event.details.ask.a.question.title", comment: ""), for: .normal)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.fromEventDetailsToAskAQuestion.rawValue,
            let lecture = sender as? Lecture,
            let navController = segue.destination as? UINavigationController,
            let destinationVC = navController.viewControllers.first as? AskAQuestionViewController {
            destinationVC.lecture = lecture
        }
        super.prepare(for: segue, sender: sender)
    }

    // MARK: - Functions

    @IBAction private func questionButtonTapped() {
        guard let identifier = lecture?.identifier else { return }
        questionDataService.canIAsk(
            for: identifier,
            success: { [weak self] enabled, lecture in
                guard let self = self else { return }

                if enabled {
                    self.performSegue(withIdentifier: Segues.fromEventDetailsToAskAQuestion.rawValue, sender: lecture)
                } else {
                    let okTitle = NSLocalizedString("already.asked.alert.ok.button.title", comment: "")
                    let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)

                    self.showAlert(
                        title: NSLocalizedString("already.asked.alert.title", comment: ""),
                        message: NSLocalizedString("already.asked.alert.message", comment: ""),
                        actions: [okAction])
                }
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.questionButtonTapped)
            }
        )
    }
}

// MARK: - UITableViewDataSource

extension EventDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let offers = lecture?.offers else {
            return 0
        }

        return offers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let offer = lecture?.offers?[safe: indexPath.row] else {
            fatalError("Invalid indexPath")
        }

        let cell: OfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: OfferTableViewCell.reuseIdentifier, for: indexPath) as! OfferTableViewCell
        cell.configureCell(offer: offer)

        return cell
    }
}
