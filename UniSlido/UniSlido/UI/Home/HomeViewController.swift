//
//  HomeViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

final class HomeViewController: UITabBarController {
    // MARK: - BaseViewControllerProtocol properties

    var isNavigationBarHidden: Bool {
        return true
    }

    // MARK: - ViewController funcitons

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        translateView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: animated)
    }

    // MARK: - BaseViewControllerProtocol functions

    func setupView() {}

    func translateView() {
        tabBar.items?[safe: 0]?.title = NSLocalizedString("tabbar.events.title", comment: "")
        tabBar.items?[safe: 1]?.title = NSLocalizedString("tabbar.questions.title", comment: "")
        tabBar.items?[safe: 2]?.title = NSLocalizedString("tabbar.info.title", comment: "")
    }
}

// MARK: - BaseViewControllerProtocol

extension HomeViewController: BaseViewControllerProtocol {}
