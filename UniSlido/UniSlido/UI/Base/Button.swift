//
//  Button.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

class Button: UIButton {
    // MARK: - Initialization

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        self.commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.commonInit()
    }

    // MARK: - Functions

    func commonInit() {
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor.blue.cgColor
        setBackgroundImage(UIImage.imageWithColor(color: UIColor.clear), for: .normal)
        setTitleColor(UIColor.blue, for: .normal)
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        layer.masksToBounds = true
        contentEdgeInsets = UIEdgeInsets(top: 7, left: 24, bottom: 7, right: 24)
    }
}
