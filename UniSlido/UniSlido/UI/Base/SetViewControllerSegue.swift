//
//  SetViewControllerSegue.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

@objc
class SetViewControllerSegue: UIStoryboardSegue {
    override func perform() {
        source.navigationController?.setViewControllers([destination], animated: true)
    }
}
