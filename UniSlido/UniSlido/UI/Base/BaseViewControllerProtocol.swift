//
//  BaseViewControllerProtocol.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 26..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol {
    var isNavigationBarHidden: Bool { get }

    func setupView()
    func translateView()
}
