//
//  BaseViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    // MARK: - BaseViewControllerProtocol properties

    var isNavigationBarHidden: Bool {
        return true
    }

    // MARK: - ViewController funcitons

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        translateView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: animated)
    }

    // MARK: - BaseViewControllerProtocol functions

    func setupView() {}

    func translateView() {}

    func showAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        actions.forEach({ alert.addAction($0) })

        present(alert, animated: true, completion: nil)
    }

    func showGeneralErrorAlert(retryBlock: (() -> Void)?) {
        let title = NSLocalizedString("general.error.alert.title", comment: "")
        let message = NSLocalizedString("general.error.alert.message", comment: "")

        var actions = [UIAlertAction]()

        let okTitle = NSLocalizedString("error.alert.ok.button.title", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        actions.append(okAction)

        if let retryBlock = retryBlock {
            let retryTitle = NSLocalizedString("error.alert.retry.button.title", comment: "")
            let retryAction = UIAlertAction(title: retryTitle, style: .default, handler: { _ in
                retryBlock()
            })
            actions.append(retryAction)
        }

        showAlert(title: title, message: message, actions: actions)
    }

    func showNetworkErrorAlert(error: Error, retryBlock: (() -> Void)?) {
        let title = NSLocalizedString("network.error.alert.title", comment: "")

        let message: String
        if case let NetworkServiceError.serverError(status) = error {
            message = status.localizedMessage
        } else {
            message = NSLocalizedString("network.error.alert.message", comment: "")
        }

        var actions = [UIAlertAction]()

        let okTitle = NSLocalizedString("error.alert.ok.button.title", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        actions.append(okAction)

        if let retryBlock = retryBlock {
            let retryTitle = NSLocalizedString("error.alert.retry.button.title", comment: "")
            let retryAction = UIAlertAction(title: retryTitle, style: .default, handler: { _ in
                retryBlock()
            })
            actions.append(retryAction)
        }

        showAlert(title: title, message: message, actions: actions)
    }

    @objc
    func toolBarDoneTapped() {
        view.endEditing(true)
    }

    func createToolbar() -> UIToolbar {
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toolBarDoneTapped))

        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.barStyle = .default
        toolbar.items = [doneBarButton]

        return toolbar
    }
}

// MARK: - BaseViewControllerProtocol

extension BaseViewController: BaseViewControllerProtocol {}
