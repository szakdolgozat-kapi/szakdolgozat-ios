//
//  CollapsableTextView.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 29..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol CollapsableTextViewDelegate: class {
    func textViewTapped(sender: CollapsableTextView)
}

class CollapsableTextView: UITextView {
    // MARK: - Properties

    weak var collapseDelegate: CollapsableTextViewDelegate?
    var heightConstraint: NSLayoutConstraint?
    var collapsedHeight: CGFloat = 105.0
    var isOpen: Bool = false {
        didSet {
            setupHeight(animated: true)
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setupView()
    }

    func calculateMaxHeight() -> CGFloat {
        let fixedWidth = frame.size.width
        let size = self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        return size.height
    }

    func setupView() {
        // setup height by content text
        setupHeight(animated: false)

        // remove padding
        contentInset = UIEdgeInsets.init(top: -4, left: -4, bottom: -4, right: -4)

        // add tap recognizer
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textViewTapped(sender:))))
    }

    @objc
    func textViewTapped(sender: UIGestureRecognizer) {
        isOpen.toggle()
        collapseDelegate?.textViewTapped(sender: self)
    }

    func setupHeight(animated: Bool) {
        let maxHeight = calculateMaxHeight()
        let height: CGFloat
        if isOpen {
            height = maxHeight < collapsedHeight ? collapsedHeight : maxHeight
        } else {
            height = collapsedHeight
        }

        if heightConstraint == nil {
            heightConstraint = NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: height)
            addConstraint(heightConstraint!)
        } else {
            heightConstraint!.constant = height
        }

        if animated {
            UIView.animate(withDuration: 0.3) {
                self.superview?.layoutIfNeeded()
            }
        } else {
            superview?.layoutIfNeeded()
        }
    }
}
