//
//  ClearableLabel.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol ClearableLabelDelegate: class {
    func labelTextDidCleared(label: ClearableLabel)
    func labelTapped(label: ClearableLabel)
}

class ClearableLabel: UILabel {
    // MARK: - Properties

    private var clearButton: UIButton!
    public weak var delegate: ClearableLabelDelegate?
    override var text: String? {
        didSet {
            clearButton.isHidden = text?.isEmpty ?? true
            if text?.isEmpty ?? true && !(oldValue?.isEmpty ?? true) {
                delegate?.labelTextDidCleared(label: self)
            }
        }
    }

    // MARK: - UILabel functions

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 4, left: 8, bottom: 4, right: #imageLiteral(resourceName: "filter_cancel").size.width + 13.0)

        super.drawText(in: rect.inset(by: insets))
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        commonInit()
    }

    // MARK: - Functions

    func commonInit() {
        let image = #imageLiteral(resourceName: "filter_cancel")
        clearButton = UIButton(type: .system)
        clearButton.translatesAutoresizingMaskIntoConstraints = false
        clearButton.setTitle("", for: .normal)
        clearButton.addTarget(self, action: #selector(clearButtonTapped), for: .touchUpInside)
        clearButton.isHidden = true
        clearButton.setBackgroundImage(image, for: .normal)
        clearButton.contentMode = .scaleToFill
        clearButton.isHidden = true
        addSubview(clearButton)
        NSLayoutConstraint.activate([
            clearButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),
            clearButton.heightAnchor.constraint(equalToConstant: 22),
            clearButton.widthAnchor.constraint(equalToConstant: 22),
            clearButton.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])

        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(labelTapped)))
    }

    @objc
    func clearButtonTapped() {
        text = ""
    }

    @objc
    func labelTapped() {
        delegate?.labelTapped(label: self)
    }
}
