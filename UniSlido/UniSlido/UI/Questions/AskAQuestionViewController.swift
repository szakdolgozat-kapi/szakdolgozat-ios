//
//  AskAQuestionViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

final class AskAQuestionViewController: BaseViewController {
    // MARK: - BaseViewControllerProtocol properties

    override var isNavigationBarHidden: Bool {
        return false
    }

    // MARK: - Properties

    @IBOutlet private weak var inputTextView: UITextView!
    @IBOutlet private weak var sendButton: Button!

    var lecture: Lecture?
    @Injected private var questionDataService: QuestionDataServiceProtocol!

    // MARK: - BaseViewControllerProtocol functions

    override func translateView() {
        super.translateView()

        title = NSLocalizedString("ask.a.question.title", comment: "")
        sendButton.setTitle( NSLocalizedString("ask.a.question.send.button.title", comment: ""), for: .normal)
    }

    override func setupView() {
        super.setupView()

        inputTextView.inputAccessoryView = createToolbar()
    }

    // MARK: - Functions

    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sendButtonTapped() {
        view.endEditing(true)
        guard let lecture = lecture else {
            return
        }

        questionDataService.askAQuestion(
            questionString: inputTextView.text,
            lectureId: lecture.identifier,
            success: { [weak self] success in
                guard let self = self else { return }

                if success {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.showGeneralErrorAlert(retryBlock: self.sendButtonTapped)
                }
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.sendButtonTapped)
            }
        )
    }
}

// MARK: - UITextViewDelegate

extension AskAQuestionViewController: UITextViewDelegate {
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
}
