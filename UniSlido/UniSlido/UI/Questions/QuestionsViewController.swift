//
//  QuestionsViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

final class QuestionsViewController: BaseViewController {
    // MARK: - Types

    private enum `Type`: String {
        case left
        case right
    }

    // MARK: - Properties

    @Injected private var questionDataService: QuestionDataServiceProtocol!

    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    @IBOutlet private weak var leftView: UIView!
    @IBOutlet private weak var leftViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var rightView: UIView!

    @IBOutlet private weak var studentLabel: UILabel!
    @IBOutlet private weak var studentInpufield: UITextField!
    @IBOutlet private weak var studentButton: Button!

    @IBOutlet private weak var teacherLabel: UILabel!
    @IBOutlet private weak var teacherInpufield: UITextField!
    @IBOutlet private weak var teacherButton: Button!

    private var selectedView: Type = .left {
        didSet {
            guard oldValue != selectedView else { return }

            updateLayout()
        }
    }

    // MARK: - BaseViewControllerProtocol functions

    override func translateView() {
        super.translateView()

        title = NSLocalizedString("questions.screen.title", comment: "")

        studentLabel.text = NSLocalizedString("questions.student.text", comment: "")
        studentButton.setTitle(NSLocalizedString("questions.student.button.title", comment: ""), for: .normal)
        segmentedControl.setTitle(NSLocalizedString("questions.student.segment.title", comment: ""), forSegmentAt: 0)

        teacherLabel.text = NSLocalizedString("questions.teacher.text", comment: "")
        teacherButton.setTitle(NSLocalizedString("questions.teacher.button.title", comment: ""), for: .normal)
        segmentedControl.setTitle(NSLocalizedString("questions.teacher.segment.title", comment: ""), forSegmentAt: 1)
    }

    override func setupView() {
        super.setupView()

        studentInpufield.inputAccessoryView = createToolbar()
        teacherInpufield.inputAccessoryView = createToolbar()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        defer { super.prepare(for: segue, sender: sender) }

        guard let lecture = sender as? Lecture else {
            return
        }

        switch segue.identifier {
        case Segues.fromQuestionsToEventDetails.rawValue:
            guard let destinationVC = segue.destination as? EventDetailsViewController else { break }
            destinationVC.lecture = lecture
        case Segues.fromQuestionsToManageQuestions.rawValue:
            guard let destinationVC = segue.destination as? ManageQuestionsViewController else { break }
            destinationVC.lecture = lecture
        default:
            break
        }
    }

    // MARK: - Functions

    @IBAction func segmentedControlChanged(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            selectedView = .left
        } else {
            selectedView = .right
        }
    }

    @IBAction func studentButtonTapped() {
        guard let lectureIDString = studentInpufield.text,
            let lectureID = Int(lectureIDString) else {
                let action = UIAlertAction(title: NSLocalizedString("invalid.questionid.asked.alert.ok.button.title", comment: ""),
                                           style: .default,
                                           handler: nil)
                showAlert(title: NSLocalizedString("invalid.questionid.alert.title", comment: ""),
                          message: NSLocalizedString("invalid.questionid.asked.alert.message", comment: ""),
                          actions: [action])
                return
        }

        questionDataService.lecture(
            by: lectureID,
            success: { [weak self] lecture in
                self?.performSegue(withIdentifier: Segues.fromQuestionsToEventDetails.rawValue, sender: lecture)
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.studentButtonTapped)
            }
        )
    }

    @IBAction func teacherButtonTapped() {
        guard let id = teacherInpufield.text else { return }

        questionDataService.startLecture(
            with: id,
            success: { [weak self] lecture in
                self?.performSegue(withIdentifier: Segues.fromQuestionsToManageQuestions.rawValue, sender: lecture)
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.teacherButtonTapped)
            }
        )
    }

    private func updateLayout() {
        switch selectedView {
        case .left:
            leftViewLeadingConstraint.constant = 0
        case .right:
            leftViewLeadingConstraint.constant = -UIScreen.main.bounds.size.width
        }

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}
