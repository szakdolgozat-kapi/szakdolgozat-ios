//
//  ManageQuestionsViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

final class ManageQuestionsViewController: BaseViewController {
    // MARK: - Properties

    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!

    @IBOutlet private weak var noQuestionContainer: UIView!
    @IBOutlet private weak var noQuestionLabel: UILabel!
    @IBOutlet private weak var reloadButton: Button!

    var lecture: Lecture?
    @Injected private var questionDataService: QuestionDataServiceProtocol!
    private var selectableQuestions = [Question]() {
        didSet {
            DispatchQueue.main.async {
                self.showNoQuestionsView(
                    self.selectableQuestions.isEmpty,
                    completion: { [weak self] _ in
                        self?.reloadData()
                    }
                )
            }
        }
    }
    var lastXPos: CGFloat = 0.0
    let pagerWidth = UIScreen.main.bounds.size.width - 32
    var actualPos: Int = 0
    var selectedPos: Int = -1

    // MARK: - BaseViewControllerProtocol properties

    override var isNavigationBarHidden: Bool {
        return false
    }

    // MARK: - BaseViewControllerProtocol functions

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchQuestions()
    }

    override func translateView() {
        title = NSLocalizedString("manage.questions.title", comment: "")

        if let askAQuestionIdentifier = lecture?.askAQuestionIdentifier {
            codeLabel.text = String(format: NSLocalizedString("manage.questions.code", comment: ""), askAQuestionIdentifier)
        } else {
            codeLabel.text = ""
        }

        titleLabel.text = NSLocalizedString("manage.questions.message", comment: "")
        noQuestionLabel.text = NSLocalizedString("manage.questions.no.question.title", comment: "")
        reloadButton.setTitle(NSLocalizedString("manage.questions.reload.button.title", comment: ""), for: .normal)
    }

    // MARK: - Functions

    private func showNoQuestionsView(_ show: Bool, completion: ((Bool) -> Void)?) {
        let alpha: CGFloat = show ? 1 : 0
        UIView.transition(
            with: noQuestionContainer,
            duration: 0.3,
            options: .transitionCrossDissolve,
            animations: {
                self.noQuestionContainer.alpha = alpha
            },
            completion: completion
        )
    }

    private func reloadData() {
        selectedPos = -1
        actualPos = 0
        lastXPos = 0.0
        collectionView.reloadData()
        if !selectableQuestions.isEmpty {
            collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: false)
        }
    }

    private func fetchQuestions() {
        guard let askAQuestionIdentifier = lecture?.askAQuestionIdentifier else { return }

        questionDataService.questions(
            for: askAQuestionIdentifier,
            success: { [weak self] questions in
                self?.selectableQuestions = questions
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.fetchQuestions)
            }
        )
    }

    private func fetchNewQuestions() {
        guard let askAQuestionIdentifier = lecture?.askAQuestionIdentifier else { return }

        var lastQuestionId: Int?
        if let lastID = selectableQuestions[safe: actualPos]?.identifier {
            lastQuestionId = lastID
        }

        questionDataService.newQuestions(
            for: askAQuestionIdentifier,
            lastQuestionId: lastQuestionId,
            success: { [weak self] questions in
                self?.selectableQuestions = questions
            },
            failure: { [weak self] error in
                guard let self = self else { return }

                self.showNetworkErrorAlert(error: error, retryBlock: self.fetchQuestions)
            }
        )
    }

    @IBAction func reloadButtonTapped(_ sender: Any) {
        fetchNewQuestions()
    }
}

extension ManageQuestionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectableQuestions.count + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "QuestionCollectionViewCell",
            for: indexPath) as? QuestionCollectionViewCell else {
            fatalError("No cell")
        }

        if let question = selectableQuestions[safe: indexPath.row] {
            let selected = selectedPos == indexPath.row
            cell.configureCell(question: question, selected: selected)
        } else {
            cell.hideContent()
        }

        return cell
    }
}

extension ManageQuestionsViewController: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x) / Int(pagerWidth)

        if page == selectableQuestions.count {
            showNoQuestionsView(true, completion: nil)
        }

        guard page > actualPos,
            let question = selectableQuestions[safe: actualPos],
            let askAQuestionIdentifier = lecture?.askAQuestionIdentifier else {
            return
        }

        questionDataService.declineQuestion(
            for: askAQuestionIdentifier,
            questionId: question.identifier,
            success: nil,
            failure: { _ in
            }
        )

        actualPos = page
        debugPrint("page changed: \(actualPos)")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Disable swipe to previous item
        if lastXPos > scrollView.contentOffset.x && Int(scrollView.contentOffset.x) % Int(pagerWidth) > Int(pagerWidth) / 2 {
            scrollView.setContentOffset(CGPoint(x: lastXPos, y: 0), animated: false)
        } else {
            lastXPos = scrollView.contentOffset.x
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row != selectableQuestions.count else { return }

        selectedPos = indexPath.row
        collectionView.reloadItems(at: [indexPath])

        guard let question = selectableQuestions[safe: indexPath.row],
            let askAQuestionIdentifier = lecture?.askAQuestionIdentifier else {
            return
        }

        questionDataService.acceptQuestion(
            for: askAQuestionIdentifier,
            questionId: question.identifier,
            success: nil,
            failure: { _ in
            }
        )
    }
}
