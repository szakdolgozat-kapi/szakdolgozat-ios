//
//  QuestionCollectionViewCell.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 30..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var acceptImageView: UIImageView!

    public static let reuseIdentifier = "QuestionCollectionViewCell"

    func configureCell(question: Question, selected: Bool) {
        textView.text = question.questionString
        textView.isHidden = false
        acceptImageView.isHidden = !selected
    }

    func hideContent() {
        textView.isHidden = true
        acceptImageView.isHidden = true
    }
}
