//
//  InfoViewController.swift
//  UniSlido
//
//  Created by Kapi Zoltán on 2019. 10. 27..
//  Copyright © 2019. Kapi Zoltán. All rights reserved.
//

import UIKit

final class InfoViewController: BaseViewController {
    // MARK: - Properties

    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var createdByTitleLabel: UILabel!
    @IBOutlet private weak var createdByLabel: UILabel!
    @IBOutlet private weak var versionLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!

    // MARK: - BaseViewControllerProtocol functions

    override func translateView() {
        super.translateView()

        infoLabel.text = NSLocalizedString("info.text", comment: "")
        createdByTitleLabel.text = NSLocalizedString("info.created.by.title", comment: "")
        createdByLabel.text = NSLocalizedString("info.created.by", comment: "")
        versionLabel.text = NSLocalizedString("info.version", comment: "")
        dateLabel.text = NSLocalizedString("info.date", comment: "")
    }
}
