//
//  IKAppDependencies.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKAppDependencies {
    
    public static let sharedInstance = IKAppDependencies()
    public var env:IKAppConstants.IKEnvironment = IKAppConstants.IKEnvironment.unknown
    
    //MARK: Application lifecycle
    
    //customization after application launch.
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        #if DEV
            self.env = IKAppConstants.IKEnvironment.dev
        #elseif MOCK
            self.env = IKAppConstants.IKEnvironment.mock
        #elseif PROD
            self.env = IKAppConstants.IKEnvironment.prod
        #endif
        
        IKConfiguration.sharedInstance.setEnv(env: self.env)
        
        return true
    }
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    //MARK: Manager Dependecies
    
    func getEventDataManager() -> IKEvenetDataManagerInterface {
        if self.env == IKAppConstants.IKEnvironment.mock {
            return IKEventDataManagerMock.sharedInstance
        }else{
            return IKEventDataManager.sharedInstance
        }
    }
    
    func getQuestionDataManager() -> IKQuestionDataManagerInterface {
        if self.env == IKAppConstants.IKEnvironment.mock {
            return IKQuestionDataManagerMock.sharedInstance
        }else{
            return IKQuestionDataManager.sharedInstance
        }
    }

    func getNetworkManager() -> NetworkManagerProtocol {
        return NetworkManager.sharedInstance
    }
    
    //MARK: UI Dependecies
    
    var tabbar: IKTabbarViewController?
    //This will be set at viewWillAppear method (after the viewDidload, if it is the first apperar!)
    var actualTab: IKBaseViewController?
    var actualTabIndex: Int?
    //This will be set at viewWillAppear method (after the viewDidload, if it is the first apperar!)
    var actualVC: IKBaseViewController?
}
