//
//  IKAppConstants.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKAppConstants {
    static let appPrefix = "IK_UniSlido_"
    
    //* segue names *//
    static let segueFromSplashToMain = "fromSplashToMain"
    static let segueFromEventListToDetails = "fromEventListToDetails"
    static let segueFromLectureDetailsToAskAQuestion = "fromEventDetailsToAskAQuestion"
    static let segueFromQuestionMainToLectureDetails = "fromQuestionsToEventDetails"
    static let segueFromQuestionMainToManageQuestions = "segueFromQuestionsToManageQuestions"
    
    
    //* Environments *//
    public enum IKEnvironment: String{
        case unknown = "unknown"
        case dev = "dev"
        case mock = "mock"
        case prod = "prod"
    }
    
    //*config keys *//
    public static let configApiURL = "ApiUrl"
    
    //*url postfixes *//
    public static let getEvents = "events"
    public static let getEvent = "event"
    public static let getLecture = "lecture"
    public static let getCanIAsk = "lecture/can_i_ask"
    public static let postAsk = "lecture/ask"
    public static let postStartLecture = "lecture/register"
    public static let getQuestions = "lecture/questions"
    public static let getNewQuestions = "lecture/new_questions"
    public static let postAcceptQuestion = "lecture/accept_question"
    public static let postDeclineQuestion = "lecture/decline_question"
    
    //* Events *//
    
    public enum IKEventFilterStatus: Int{
        case open = 0
        case closed = 1
    }
    
    public enum IKEventFilterDateLabel: String{
        case start = "start"
        case end = "end"
    }
    
    public static let eventFilterAnimationDuration: TimeInterval = 0.33
    
    public static let eventFilterClosedHeight: CGFloat = 53.0
    public static let eventFilterOpenHeight: CGFloat = 164.0

}
