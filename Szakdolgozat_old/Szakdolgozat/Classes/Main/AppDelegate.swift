//
//  AppDelegate.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 04..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        return IKAppDependencies.sharedInstance.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        IKAppDependencies.sharedInstance.applicationWillResignActive(application)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        IKAppDependencies.sharedInstance.applicationDidEnterBackground(application)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        IKAppDependencies.sharedInstance.applicationWillEnterForeground(application)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        IKAppDependencies.sharedInstance.applicationDidBecomeActive(application)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        IKAppDependencies.sharedInstance.applicationWillTerminate(application)
    }


}

