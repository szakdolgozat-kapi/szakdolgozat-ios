//
//  IKCheckbox.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol IKCheckBoxDelegate: class {
    func chechboxStateChanged(checked: Bool)
}

class IKCheckBox: UIButton {
    // Images
    let checkedImage = #imageLiteral(resourceName: "checkbox_checked")
    let uncheckedImage = #imageLiteral(resourceName: "checkbox_unchecked")
    var delegate: IKCheckBoxDelegate?
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setBackgroundImage(checkedImage, for: .normal)
            } else {
                self.setBackgroundImage(uncheckedImage, for: .normal)
            }
            delegate?.chechboxStateChanged(checked: isChecked)
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(IKCheckBox.buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: Any?) {
        isChecked = !isChecked
    }
}
