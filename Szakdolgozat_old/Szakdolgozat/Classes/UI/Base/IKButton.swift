//
//  IKButton.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor.blue.cgColor
        setBackgroundImage(UIImage.imageWithColor(color: UIColor.clear), for: .normal)
        setTitleColor(UIColor.blue, for: .normal)
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        layer.masksToBounds = true
    }

}
