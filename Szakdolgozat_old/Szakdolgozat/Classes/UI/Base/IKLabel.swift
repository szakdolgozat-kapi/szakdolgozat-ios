//
//  IKLabel.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 12..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol IKLabelDelegate: class {
    func labelTextDidDeleted(label: IKLabel!)
}

class IKLabel: UILabel {
    
    private var clearButton:UIButton!
    public var delegate:IKLabelDelegate?
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: #imageLiteral(resourceName: "filter_cancel_icon").size.width + 5.0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        addObserver(self, forKeyPath: "text", options: [.new, .old], context: nil)
        
        clearButton = UIButton(type: .system)
        clearButton.setTitle("", for: .normal)
        clearButton.addTarget(self, action: #selector(IKLabel.clearButtonTapped), for: .touchUpInside)
        clearButton.isHidden = true
        clearButton.setBackgroundImage(#imageLiteral(resourceName: "filter_cancel_icon"), for: .normal)
        clearButton.frame = CGRect(x: self.frame.size.width - #imageLiteral(resourceName: "filter_cancel_icon").size.width,
                                   y: (self.frame.size.height - #imageLiteral(resourceName: "filter_cancel_icon").size.height) / 2,
                                   width: #imageLiteral(resourceName: "filter_cancel_icon").size.width,
                                   height: #imageLiteral(resourceName: "filter_cancel_icon").size.height)
        addSubview(clearButton)
        
    }
    
    @objc func clearButtonTapped(){
        self.text = ""
        delegate?.labelTextDidDeleted(label: self)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "text" {
            if (text != nil && text!.isEmpty == false){
                clearButton.isHidden = false
            } else {
                clearButton.isHidden = true
            }
        }
    }

}
