//
//  IKCollepsableTextView.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

protocol IKCollapsableTextViewDelegate: class {
    func textViewTapped(sender: IKCollapsableTextView)
}

class IKCollapsableTextView: UITextView {
    
    var collapseDelegate: IKCollapsableTextViewDelegate?
    var heightConstraint: NSLayoutConstraint?
    var arrowTopConstraint: NSLayoutConstraint?
    var collapsedHeight: CGFloat = 105.0
    let arrow = UIImageView(image: #imageLiteral(resourceName: "down_icon"))
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupView()
    }
    
    func calculateMaxHeight() -> CGFloat {
        let fixedWidth = frame.size.width
        let size = self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        return size.height
    }
    
    func setupView(){
        //setup height by content text
        var height: CGFloat = 0.0
        if !text.isEmpty {
            let maxHeight = calculateMaxHeight()
            height = maxHeight < collapsedHeight ? maxHeight : collapsedHeight
        }
        setHeight(height: height)
        
        //remove padding
        contentInset = UIEdgeInsets.init(top: -4, left: -4, bottom: -4, right: -4)
        
        //add image indicator
        
        arrow.translatesAutoresizingMaskIntoConstraints = false
        addSubview(arrow)
        clipsToBounds = false
        
        NSLayoutConstraint(item: arrow, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: arrow.image!.size.width).isActive = true
        NSLayoutConstraint(item: arrow, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: arrow.image!.size.height).isActive = true
        
        NSLayoutConstraint(item: arrow, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: frame.size.width - (arrow.image!.size.width / 2)).isActive = true
        
        arrowTopConstraint = NSLayoutConstraint(item: arrow, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: height - (arrow.image!.size.height / 2) )
        arrowTopConstraint?.isActive = true
        
        if height == 0 {
            arrow.isHidden = true
        }
        
        //add tap recognizer
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(IKCollapsableTextView.textViewTapped(sender:))))
        
        addObserver(self, forKeyPath: "text", options: [.new, .old], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "text" {
            if (text != nil && text!.isEmpty == false){
                arrow.isHidden = false
                
                var height: CGFloat = 0.0
                if !text.isEmpty {
                    let maxHeight = calculateMaxHeight()
                    height = maxHeight < collapsedHeight ? maxHeight : collapsedHeight
                }
                setHeight(height: height)
                arrowTopConstraint?.constant = height - (arrow.image!.size.height / 2)
            } else {
                arrow.isHidden = true
                setHeight(height: 0)
            }
        }
    }
    
    @objc func textViewTapped(sender: UIGestureRecognizer){
        if arrow.image == #imageLiteral(resourceName: "down_icon") {
            arrow.image = #imageLiteral(resourceName: "up_icon")
            let height = calculateMaxHeight()
            setHeight(height: height)
            arrowTopConstraint?.constant = height - (arrow.image!.size.height / 2)
        } else {
            arrow.image = #imageLiteral(resourceName: "down_icon")
            var height: CGFloat = 0.0
            if !text.isEmpty {
                let maxHeight = calculateMaxHeight()
                height = maxHeight < collapsedHeight ? maxHeight : collapsedHeight
            }
           setHeight(height: height)
            arrowTopConstraint?.constant = height - (arrow.image!.size.height / 2)
        }
        collapseDelegate?.textViewTapped(sender: self)
        
    }
    
    func setHeight(height: CGFloat){
        if heightConstraint == nil{
            heightConstraint = NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1.0,
                                                  constant: height)
            heightConstraint!.priority = UILayoutPriority(rawValue: 999)
            addConstraint(heightConstraint!)
        } else {
            heightConstraint!.constant = height
        }
    }

}
