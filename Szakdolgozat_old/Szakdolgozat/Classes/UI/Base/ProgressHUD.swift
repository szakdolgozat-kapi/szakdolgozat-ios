//
//  ProgressHUD.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProgressHUD: NSObject {
    private var hud: MBProgressHUD!
    
    convenience init(view: UIView, title: String? = nil, showIndicator: Bool = true) {
        self.init()
        
        self.hud = MBProgressHUD.showAdded(to: view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.bezelView.color = UIColor(white: 0.4, alpha: 0.4)
        
        if title != nil {
            if showIndicator == false {
                self.hud.mode = MBProgressHUDMode.text
            }
            
            self.hud.label.text = title
        }
    }
    
    func hide() {
        self.hud.hide(animated: true)
    }
}
