//
//  IKBaseViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKBaseViewController: UIViewController {
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension IKBaseViewController: IKBaseViewControllerProtocol{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IKAppDependencies.sharedInstance.actualVC = self
    }
}
