//
//  IKEventDetailsViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKLectureDetailsViewController: IKBaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelHeight: NSLayoutConstraint!

    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventNameLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var placeLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionTextView: IKCollapsableTextView!
    
    @IBOutlet weak var questionButton: IKButton!
    @IBOutlet weak var questionButtonHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var imageView: CachedImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    let questionDataManager = IKAppDependencies.sharedInstance.getQuestionDataManager()
    var firtAppear: Bool = true
    
    var lecture: IKLecture?{
        didSet{
            setupUI()
        }
    }
    
    func setupUI(){
        tableView?.reloadData()
        
        if (lecture?.name != nil && lecture!.name!.isEmpty == false){
            titleLabel?.text = lecture?.name
        } else {
            titleLabelHeight?.constant = 0
        }
        
        if (lecture?.eventName != nil && lecture!.eventName!.isEmpty == false){
            eventNameLabel?.text = lecture?.eventName
        } else {
            eventNameLabelHeight?.constant = 0
        }
        
        if (lecture?.placeName != nil && lecture!.placeName!.isEmpty == false){
            placeLabel?.text = lecture?.placeName
        } else {
            placeLabelHeight?.constant = 0
        }
        
        descriptionTextView?.text = lecture?.info
        
        if (lecture?.askAQuestionIdentifier != nil && lecture!.askAQuestionIdentifier!.isEmpty == false){
            questionButtonHeight?.constant = 40
            
            questionButton?.setTitle(NSLocalizedString("lecture.details.ask.a.question.title", comment: "Ask a question"), for: .normal)
            
            questionButton?.isHidden = false
        }else{
            questionButtonHeight?.constant = 0
            questionButton?.isHidden = true
        }
        
        if let url = lecture?.imageUrl{
            imageView?.emptyImage = #imageLiteral(resourceName: "ik_logo")
            imageView?.loadImage(urlString: url)
        }else{
            imageView?.image = #imageLiteral(resourceName: "ik_logo")
        }
        
        tableViewHeight?.constant = tableView.contentSize.height
        view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = NSLocalizedString("lecture.details.title", comment: "Lecture details title")
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        descriptionTextView.collapseDelegate = self
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard !firtAppear else {
            firtAppear = false
            return
        }
        
        if (lecture?.identifier != nil && lecture!.identifier!.isEmpty == false){
            let hud = ProgressHUD(view: self.view)
            questionDataManager.getLecture(id: lecture!.identifier!) { (lect, error) in
                hud.hide()
                if (error != nil && lect != nil){
                    self.lecture = lect
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == IKAppConstants.segueFromLectureDetailsToAskAQuestion && sender is String){
            let destinationVC = segue.destination as! IKAskAQuestionViewController
            destinationVC.lectureIdentifier = sender as? String
        }
        super.prepare(for: segue, sender: sender)
    }
    
    @IBAction func questionButtonTapped(_ sender: Any) {
        if (lecture?.askAQuestionIdentifier != nil && lecture!.askAQuestionIdentifier!.isEmpty == false){
            let hud = ProgressHUD(view: self.view)
            questionDataManager.canIAsk(lectureId: lecture!.askAQuestionIdentifier!, closure: { (canAsk, lecture, error) in
                hud.hide()
                guard error == nil else{
                    //TODO: show error message
                    let errorAlert = UIAlertController(title: NSLocalizedString("error.alert.title", comment: "Error title"), message: error?.domain, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: NSLocalizedString("error.alert.ok.button.title", comment: "Ok"), style: .default, handler: { (alertAction) in
                        
                    })
                    errorAlert.addAction(action)
                    
                    self.present(errorAlert, animated: true, completion: nil)
                    
                    
                    
                    return
                }
                
                if canAsk == true{
                    self.performSegue(withIdentifier: IKAppConstants.segueFromLectureDetailsToAskAQuestion, sender: lecture?.askAQuestionIdentifier)
                }else{
                    // show message, 'You already asked a question in 5 minutes'
                    
                    let errorAlert = UIAlertController(title: NSLocalizedString("error.alert.title", comment: ""), message: NSLocalizedString("already.ask.alert.message", comment: ""), preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: NSLocalizedString("already.ask.alert.ok.button.title", comment: ""), style: .default, handler: { (alertAction) in
                        
                    })
                    errorAlert.addAction(action)
                    self.present(errorAlert, animated: true, completion: nil)
                    
                }
            })
        }
    }
}

extension IKLectureDetailsViewController: UITableViewDataSource{
    
    //MARK: UITableView data source
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (lecture == nil || lecture!.teachersOffering == nil || lecture!.teachersOffering!.count == 0) {
            return 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (lecture == nil || lecture!.teachersOffering == nil || lecture!.teachersOffering!.count == 0) {
            return 0
        } else {
            return lecture!.teachersOffering!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offer = lecture!.teachersOffering![indexPath.row]
        
        let cell: IKTeachersOfferingTableViewCell = tableView.dequeueReusableCell(withIdentifier: IKTeachersOfferingTableViewCell.reuseIdentifier, for: indexPath) as! IKTeachersOfferingTableViewCell
        cell.configureCell(offer: offer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 16.0, y: 0.0, width: UIScreen.main.bounds.size.width - 16.0, height: 44.0))
        headerView.backgroundColor = UIColor.lightGray
        
        let label = UILabel(frame: headerView.frame)
        label.text = NSLocalizedString("lecture.details.offers.title", comment: "Teachers offering")
        headerView.addSubview(label)
        
        return headerView
    }
}

extension IKLectureDetailsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        //TODO show an alert to show the whole text
    }
}

extension IKLectureDetailsViewController: IKCollapsableTextViewDelegate{
    func textViewTapped(sender: IKCollapsableTextView) {
        UIView.animate(withDuration: 0.3) { 
            self.view.layoutIfNeeded()
        }
    }
}
