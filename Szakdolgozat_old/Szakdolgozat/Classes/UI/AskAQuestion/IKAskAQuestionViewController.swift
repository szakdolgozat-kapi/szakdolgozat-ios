//
//  IKAskAQuestionViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKAskAQuestionViewController: IKBaseViewController {
    
    var lectureIdentifier: String!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sendButton: IKButton!
    
    let questionDataManager = IKAppDependencies.sharedInstance.getQuestionDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        titleLabel.text = NSLocalizedString("ask.title.text", comment: "Ask a question title")
        inputTextView.text = ""
        inputTextView.layer.cornerRadius = 5
        inputTextView.layer.borderWidth = 1
        inputTextView.layer.borderColor = UIColor.blue.cgColor
        inputTextView.becomeFirstResponder()
        sendButton.setTitle( NSLocalizedString("ask.send.button.title", comment: "Send question button title"), for: .normal)
    }

    @IBAction func sendButtonTapped(_ sender: Any) {
        let question = IKQuestion()
        question.lectureId = lectureIdentifier
        question.questionString = inputTextView.text
        
        let hud = ProgressHUD(view: self.view)
        questionDataManager.askAQuestion(question: question) { (success, error) in
            hud.hide()
            guard error == nil else{
                //TODO: show error alert
                
                return
            }
            
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
}
