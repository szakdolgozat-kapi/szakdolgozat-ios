//
//  IKTeachersOfferingTableViewCell.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKTeachersOfferingTableViewCell: UITableViewCell {
    
    public static let reuseIdentifier = "IKTeachersOfferingTableViewCell"
    
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var courseNameLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(offer: IKTeachersOffering) {
        teacherNameLabel.text = offer.teacher
        courseNameLabel.text = offer.course
        offerLabel.text = offer.offer
    }

}
