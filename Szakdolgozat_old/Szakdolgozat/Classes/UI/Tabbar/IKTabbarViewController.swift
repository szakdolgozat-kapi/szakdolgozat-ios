//
//  Tabbar.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKTabbarViewController : UITabBarController{
    
    var initialEvents: Array<IKEvent>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IKAppDependencies.sharedInstance.tabbar = self
        IKAppDependencies.sharedInstance.actualTabIndex = 0
        
        //find IKEventsViewController, and set initial events array
        if let events = initialEvents{
            viewControllers?.forEach({ (navController) in
                let nVC = navController as! UINavigationController
                debugPrint("nVC.viewControllers.count: \(nVC.viewControllers.count)")
                if (nVC.viewControllers.count >= 1){
                    let rootVC = nVC.viewControllers[nVC.viewControllers.count - 1]
                    if rootVC is IKEventsViewController{
                        let eventsVC = rootVC as! IKEventsViewController
                        eventsVC.allEvent = events
                    }
                }
            })
        }
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        IKAppDependencies.sharedInstance.actualTabIndex = (tabBar.items! as [UITabBarItem]).index(of: item)
        
    }
    
}
