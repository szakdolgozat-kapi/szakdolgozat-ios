//
//  IKInfoViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKInfoViewController: IKBaseViewController {
    
    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var envirionmentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = NSLocalizedString("info.screen.title", comment: "Info title")
        infoTextView.text = NSLocalizedString("info.text", comment: "Info text")
        versionLabel.text = NSLocalizedString("info.version", comment: "Info text") + " " + (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
        
        let env = IKAppDependencies.sharedInstance.env
        switch env {
        case .mock:
            envirionmentLabel.isHidden = false
            envirionmentLabel.text = env.rawValue
        case .dev:
            envirionmentLabel.isHidden = false
            envirionmentLabel.text = env.rawValue
        case .prod:
            envirionmentLabel.isHidden = true
        default: break
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IKAppDependencies.sharedInstance.actualTab = self
    }
}
