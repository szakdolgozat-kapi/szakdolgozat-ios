//
//  IKQuestionsViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKQuestionMainViewController: IKBaseViewController {

    @IBOutlet weak var inputboxLeftConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var askTitleLabel: UILabel!
    @IBOutlet weak var askInputField: UITextField!
    @IBOutlet weak var askSendButton: IKButton!
    
    @IBOutlet weak var teacherTitleLabel: UILabel!
    @IBOutlet weak var teacherInputField: UITextField!
    @IBOutlet weak var teacherSendButton: IKButton!
    
    let questionDataManager = IKAppDependencies.sharedInstance.getQuestionDataManager()
    
    var endEditRecognizer:UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        endEditRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKQuestionMainViewController.endEditInView(sender:)))

        navigationItem.title = NSLocalizedString("question.screen.title", comment: "Question title")
        segmentedControl.setTitle(NSLocalizedString("question.segment.student", comment: "Student"), forSegmentAt: 0)
        segmentedControl.setTitle(NSLocalizedString("question.segment.teacher", comment: "Teacher"), forSegmentAt: 1)
        askTitleLabel.text = NSLocalizedString("question.student.title", comment: "Student title")
        teacherTitleLabel.text = NSLocalizedString("question.teacher.title", comment: "Teacher title")
        askSendButton.setTitle(NSLocalizedString("question.send.button.title", comment: "Send"), for: .normal)
        teacherSendButton.setTitle(NSLocalizedString("question.send.button.title", comment: "Send"), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(IKQuestionMainViewController.addGestureRecognizer), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IKAppDependencies.sharedInstance.actualTab = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == IKAppConstants.segueFromQuestionMainToLectureDetails && sender is IKLecture){
            let destinationVC = segue.destination as! IKLectureDetailsViewController
            destinationVC.lecture = sender as? IKLecture
        } else if (segue.identifier == IKAppConstants.segueFromQuestionMainToManageQuestions && sender is IKLecture){
            let destinationVC = segue.destination as! IKManageQuestionsViewController
            destinationVC.lecture = sender as? IKLecture
        }
        super.prepare(for: segue, sender: sender)
    }
    
    @IBAction func segmentedControlChanged(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            inputboxLeftConstraint.constant = -16
        } else {
            inputboxLeftConstraint.constant = -UIScreen.main.bounds.size.width - 16
        }
        UIView.animate(withDuration: 0.33) { 
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func askSendButtonTapped(_ sender: Any) {
        if let lectureID = askInputField.text{
            let hud = ProgressHUD(view: self.view)
            questionDataManager.getLecture(id: lectureID, closure: { (lecture, error) in
                hud.hide()
                guard error == nil else{
                    //TODO: show error message
                    return
                }
                
                self.performSegue(withIdentifier: IKAppConstants.segueFromQuestionMainToLectureDetails, sender: lecture)
            })
        }
    }
    
    @IBAction func teacherSendButtonTapped(_ sender: Any) {
        if let identifier = teacherInputField.text{
            let hud = ProgressHUD(view: self.view)
            questionDataManager.startLecture(identifier: identifier, closure: { (lecture, error) in
                hud.hide()
                guard error == nil && lecture != nil else{
                    //TODO: show error message
                    return
                }
                
                self.performSegue(withIdentifier: IKAppConstants.segueFromQuestionMainToManageQuestions, sender: lecture)
            })
        }
    }
    
    @objc func addGestureRecognizer() {
        self.view.addGestureRecognizer(endEditRecognizer)
    }
    
    @objc func endEditInView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.view.removeGestureRecognizer(endEditRecognizer)
    }
    
}
