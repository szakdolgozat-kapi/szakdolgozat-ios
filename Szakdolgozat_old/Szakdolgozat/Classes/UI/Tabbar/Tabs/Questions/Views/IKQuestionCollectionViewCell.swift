//
//  IKQuestionCollectionViewCell.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 15..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKQuestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var acceptImageView: UIImageView!
    
    public static let reuseIdentifier = "IKQuestionCollectionViewCell"
    
    func configureCell(question: IKQuestion, selected: Bool){
        textView.text = question.questionString
        textView.isHidden = false
        acceptImageView.isHidden = !selected
    }
    
    func hideContent(){
        textView.isHidden = true
        acceptImageView.isHidden = true
    }
}
