//
//  IKManageQuestionsViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 15..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKManageQuestionsViewController: IKBaseViewController {
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var noQuestionContainer: UIView!
    @IBOutlet weak var noQuestionLabel: UILabel!
    @IBOutlet weak var reloadButton: IKButton!
    
    var lecture: IKLecture!
    var dataSource: Array<IKQuestion>?{
        didSet{
            guard dataSource != nil else {
                return
            }
            
            if dataSource!.count > 0 {
                //hide no elements view
                UIView.transition(with: noQuestionContainer,
                                  duration: 0.2,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.noQuestionContainer.isHidden = true
                }, completion: {(completed) in
                    self.reloadData()})
            }else{
                //show no elements view
                UIView.transition(with: noQuestionContainer,
                                  duration: 0.4,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.noQuestionContainer.isHidden = false
                }, completion: {(completed) in
                    self.reloadData()})
            }
        }
    }
    var lastXPos: CGFloat = 0.0
    let pagerWidth = UIScreen.main.bounds.size.width - 32
    var actualPos: Int = 0
    var selectedPos: Int = -1
    
    let questionDataManager = IKAppDependencies.sharedInstance.getQuestionDataManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .horizontal
        }
        
        codeLabel.text = NSLocalizedString("manage.questions.code", comment: "code") + lecture.askAQuestionIdentifier!
        titleLabel.text = NSLocalizedString("manage.questions.title", comment: "title")
        noQuestionLabel.text = NSLocalizedString("manage.questions.no.question.title", comment: "no questions")
        reloadButton.setTitle(NSLocalizedString("manage.questions.reload.button.title", comment: "reload"), for: .normal)
        
        loadQuestions()
    }
    
    func loadQuestions() {
        let hud = ProgressHUD.init(view: self.view)
        questionDataManager.getQuestions(lectureId: lecture.askAQuestionIdentifier!) { (questions, error) in
            hud.hide()
            guard error == nil else{
                //TODO: show error message
                
                return
            }
            
            var downloadedQuestions = Array<IKQuestion>()
            if let q = questions{
                downloadedQuestions = q
            }
            
            self.dataSource = downloadedQuestions
        }
    }
    
    func loadNewQuestions() {
        let hud = ProgressHUD.init(view: self.view)
        var lastQuestionId:String?
        if (dataSource != nil && dataSource!.count-1 >= actualPos) {
            lastQuestionId = dataSource?[actualPos].identifier!
        }
        
        questionDataManager.getNewQuestions(lectureId: lecture.askAQuestionIdentifier!, lastQuestionId: lastQuestionId) { (questions, error) in
            hud.hide()
            guard error == nil else{
                //TODO: show error message
                
                return
            }
            
            var downloadedQuestions = Array<IKQuestion>()
            if let q = questions{
                downloadedQuestions = q
            }
            
            self.dataSource = downloadedQuestions
        }
    }

    @IBAction func reloadButtonTapped(_ sender: Any) {
        loadNewQuestions()
    }
    
}

extension IKManageQuestionsViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource != nil && dataSource!.count > 0 ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource != nil ? dataSource!.count + 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: IKQuestionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: IKQuestionCollectionViewCell.reuseIdentifier, for: indexPath) as! IKQuestionCollectionViewCell
        
        if indexPath.row != dataSource!.count{
            let question = dataSource![indexPath.row]
            let selected = selectedPos == indexPath.row
            cell.configureCell(question: question, selected: selected)
        } else {
            cell.hideContent()
        }
        
        return cell
    }
    
    func reloadData() {
        selectedPos = -1
        actualPos = 0
        lastXPos = 0.0
        collectionView.reloadData()
        if dataSource != nil && dataSource!.count > 0{
            let ip = IndexPath(row: 0, section: 0)
            collectionView.scrollToItem(at: ip, at: .left, animated: false)
        }
        
    }
    
}

extension IKManageQuestionsViewController: UICollectionViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x) / Int(pagerWidth)
        
        if page == dataSource?.count{
            //show no elements view
            UIView.transition(with: noQuestionContainer,
                              duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.noQuestionContainer.isHidden = false
            }, completion: {(completed) in
                })
        }
        
        if page > actualPos{
            let question = dataSource![actualPos]
            questionDataManager.declineQuestion(lectureId: lecture.askAQuestionIdentifier!, questionId: question.identifier!) { (success, error) in
                if (error != nil){
                    //TODO: show error screen
                    
                }
            }
            
            actualPos = page
            debugPrint("page changed: \(actualPos)")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Disable swipe to previous item
        if (lastXPos > scrollView.contentOffset.x && Int(scrollView.contentOffset.x) % Int(pagerWidth) > Int(pagerWidth) / 2 ) {
            scrollView.setContentOffset(CGPoint(x: lastXPos, y: 0), animated: false)
        }else{
            lastXPos = scrollView.contentOffset.x
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row != dataSource!.count else {
            return
        }
        selectedPos = indexPath.row
        collectionView.reloadItems(at: [indexPath])
        
        let question = dataSource![indexPath.row]
        questionDataManager.acceptQuestion(lectureId: lecture.askAQuestionIdentifier!, questionId: question.identifier!) { (success, error) in
            if (error != nil){
                //TODO: show error screen
                
            }
        }
    }
    
}

