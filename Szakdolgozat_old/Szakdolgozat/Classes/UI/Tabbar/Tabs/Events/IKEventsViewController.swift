//
//  IKEventsViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKEventsViewController: IKBaseViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterContainerView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var filterContainerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var startDateTitleLabel: UILabel!
    @IBOutlet weak var startDateLabel: IKLabel!

    @IBOutlet weak var endDateTitleLabel: UILabel!
    @IBOutlet weak var endDateLabel: IKLabel!

    @IBOutlet weak var teachersOfferingTitleLabel: UILabel!
    @IBOutlet weak var teachersOfferingCheckBox: IKCheckBox!

    @IBOutlet weak var noElementsContainer: UIView!
    @IBOutlet weak var noElementsLabel: UILabel!
    var eventFilter = IKEventFilter(){
        didSet{
            debugPrint("old value: \(oldValue.description)")
            debugPrint("new value: \(eventFilter.description)")
            self.doFilter()
        }
    }
    
    let eventDataManager = IKAppDependencies.sharedInstance.getEventDataManager()
    var firstAppear: Bool = true
    
    var allEvent: Array<IKEvent> = Array<IKEvent>(){
        didSet{
            guard !firstAppear else {
                dataSource = Array(allEvent)
                return
            }
            
            doFilter()
        }
    }
    var dataSource: Array<IKEvent> = Array<IKEvent>(){
        didSet{
            guard !firstAppear else {
                return
            }
            
            if dataSource.count > 0 {
                //hide no elements view
                UIView.transition(with: noElementsContainer,
                                  duration: 0.2,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.noElementsContainer.isHidden = true
                }, completion: {(completed) in
                    self.tableView.reloadData()})
            }else{
                //show no elements view
                UIView.transition(with: noElementsContainer,
                                  duration: 0.4,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.noElementsContainer.isHidden = false
                }, completion: {(completed) in
                    self.tableView.reloadData()})
            }
        }
    }
    
    
    var picker: DateTimePicker?
    
    var endEditRecognizer: UITapGestureRecognizer!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        IKAppDependencies.sharedInstance.actualTab = self
        
        guard !firstAppear else {
            firstAppear = false
            return
        }
        
        //set datasource
        let hud = ProgressHUD(view: self.view)
        eventDataManager.getAllEvent { (events, error) in
            hud.hide()
            var downloadedEvents = Array<IKEvent>()
            if let e = events{
                downloadedEvents = e
            }
            
            if let e = error{
                //TODO: show error alert
                
                debugPrint("error: \(e.domain)")
            }
            
            self.allEvent = downloadedEvents
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        startDateTitleLabel.text = NSLocalizedString("events.start.date.title.text", comment: "Start date title")
        endDateTitleLabel.text = NSLocalizedString("events.end.date.title.text", comment: "End date title")
        
        teachersOfferingTitleLabel.text = NSLocalizedString("events.teachers.offering.title.text", comment: "Teachers offering")
        teachersOfferingCheckBox.delegate = self
        
        noElementsLabel.text = NSLocalizedString("events.filter.no.results", comment: "No results")
        
        searchBar.setBackgroundImage(UIImage.imageWithColor(color: UIColor.clear), for: .any, barMetrics: .default)
        
        //setup filter box
        filterButton.tag = IKAppConstants.IKEventFilterStatus.closed.rawValue
        filterContainerViewHeight.constant = IKAppConstants.eventFilterClosedHeight
        filterButton.setBackgroundImage(#imageLiteral(resourceName: "filter_icon_black"), for: .normal)
        
        endEditRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKEventsViewController.endEditInView(sender:)))
        
        let startDateRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKEventsViewController.startOrEndDateTapped(sender:)))
        startDateRecognizer.anyTag = IKAppConstants.IKEventFilterDateLabel.start as Any?
        startDateLabel.addGestureRecognizer(startDateRecognizer)
        startDateLabel.anyTag = IKAppConstants.IKEventFilterDateLabel.start as Any?
        startDateLabel.delegate = self
        
        let startDateTitleRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKEventsViewController.startOrEndDateTapped(sender:)))
        startDateTitleRecognizer.anyTag = IKAppConstants.IKEventFilterDateLabel.start as Any?
        startDateTitleLabel.addGestureRecognizer(startDateTitleRecognizer)
        
        let endDateRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKEventsViewController.startOrEndDateTapped(sender:)))
        endDateRecognizer.anyTag = IKAppConstants.IKEventFilterDateLabel.end as Any?
        endDateLabel.addGestureRecognizer(endDateRecognizer)
        endDateLabel.anyTag = IKAppConstants.IKEventFilterDateLabel.end as Any?
        endDateLabel.delegate = self
        
        let endDateTitleRecognizer = UITapGestureRecognizer(target: self, action: #selector(IKEventsViewController.startOrEndDateTapped(sender:)))
        endDateTitleRecognizer.anyTag = IKAppConstants.IKEventFilterDateLabel.end as Any?
        endDateTitleLabel.addGestureRecognizer(endDateTitleRecognizer)
        
        let teachersOfferingTitleRecognizer = UITapGestureRecognizer(target: teachersOfferingCheckBox, action: #selector(IKCheckBox.buttonClicked(sender:)))
        teachersOfferingTitleLabel.addGestureRecognizer(teachersOfferingTitleRecognizer)
    }
    
    func doFilter(){
        guard allEvent.count > 0 else {
            dataSource = Array<IKEvent>()
            return
        }
        
        var filteredArray = allEvent.map({$0.copy()})
        
        // search in lecture's name and info
        var tmp = Array(filteredArray)
        guard tmp.count > 0 else {
            dataSource = Array<IKEvent>()
            return
        }
        
        for i in 0...filteredArray.count-1{
            let event = filteredArray[i]
            
            if (eventFilter.name != nil && !eventFilter.name!.isEmpty){
                let l = event.lectures!.filter({
                    ($0.name != nil &&
                        $0.name!.lowercased().contains(eventFilter.name!.lowercased())) ||
                        $0.info != nil &&
                        $0.info!.lowercased().contains(eventFilter.name!.lowercased())
                })

                let index = tmp.index(of: event)
                if (l.count == 0){
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = Array(l)
                }
                
            }
        }
        filteredArray = Array(tmp)
        
        
        //filter for lecture's start date
        tmp = Array(filteredArray)
        guard tmp.count > 0 else {
            dataSource = Array<IKEvent>()
            return
        }
        
        for i in 0...filteredArray.count-1{
            let event = filteredArray[i]
            
            if (eventFilter.start != nil && eventFilter.start! > 0 ){
                let l = event.lectures!.filter({
                    $0.startTimeStamp != nil &&
                    $0.startTimeStamp! >= eventFilter.start!
                })
                
                let index = tmp.index(of: event)
                if (l.count == 0){
                    tmp.remove(at: index!)
                } else {
                    tmp[index!].lectures = Array(l)
                }
            }
        }
        filteredArray = Array(tmp)
        
        //filter for lecture's end date
        tmp = Array(filteredArray)
        guard tmp.count > 0 else {
            dataSource = Array<IKEvent>()
            return
        }
        
        for i in 0...filteredArray.count-1{
            let event = filteredArray[i]
            
            if (eventFilter.end != nil && eventFilter.end! > 0 ){
                let l = event.lectures!.filter({
                    $0.endTimeStamp != nil &&
                        $0.endTimeStamp! <= eventFilter.end!
                })
                
                let index = tmp.index(of: event)
                if (l.count == 0){
                    tmp.remove(at: index!)
                } else {
                    
                    tmp[index!].lectures = Array(l)
                }
            }
        }
        filteredArray = Array(tmp)
        
        //filter for lecture's teachers offering
        tmp = Array(filteredArray)
        guard tmp.count > 0 else {
            dataSource = Array<IKEvent>()
            return
        }
        
        for i in 0...filteredArray.count-1{
            let event = filteredArray[i]
            
            if (eventFilter.teachersOffering != nil && eventFilter.teachersOffering! == true ){
                let l = event.lectures!.filter({
                    $0.teachersOffering != nil &&
                        $0.teachersOffering!.count > 0
                })
                
                let index = tmp.index(of: event)
                if (l.count == 0){
                    tmp.remove(at: index!)
                } else {
                    
                    tmp[index!].lectures = Array(l)
                }
            }
        }
        filteredArray = Array(tmp)
        
        dataSource = Array(filteredArray)
    }
    
    @IBAction func filterButtonTapped(_ sender: Any) {
        let status = IKAppConstants.IKEventFilterStatus(rawValue: filterButton.tag)
        switch status! {
        case .closed:
            filterButton.tag = IKAppConstants.IKEventFilterStatus.open.rawValue
            filterContainerViewHeight.constant = IKAppConstants.eventFilterOpenHeight
            filterButton.setBackgroundImage(#imageLiteral(resourceName: "filter_icon_gray"), for: .normal)
            break
        case .open:
            filterButton.tag = IKAppConstants.IKEventFilterStatus.closed.rawValue
            filterContainerViewHeight.constant = IKAppConstants.eventFilterClosedHeight
            filterButton.setBackgroundImage(#imageLiteral(resourceName: "filter_icon_black"), for: .normal)
            
            break
        }
        UIView.animate(withDuration: IKAppConstants.eventFilterAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == IKAppConstants.segueFromEventListToDetails && sender is IKLecture) {
            let destinationVC = segue.destination as! IKLectureDetailsViewController
            destinationVC.lecture = sender as? IKLecture
        }
        super.prepare(for: segue, sender: sender)
    }
}

extension IKEventsViewController: UITableViewDataSource{

    //MARK: UITableView data source
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].lectures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let lecture = dataSource[indexPath.section].lectures[indexPath.row]
        
        let cell: IKEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: IKEventTableViewCell.reuseIdentifier, for: indexPath) as! IKEventTableViewCell
        cell.configureCell(lecture: lecture)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 16.0, y: 0.0, width: UIScreen.main.bounds.size.width - 16.0, height: 44.0))
        headerView.backgroundColor = UIColor.lightGray
        
        let label = UILabel(frame: headerView.frame)
        let event = dataSource[section]
        label.text = event.name
        headerView.addSubview(label)
        
        return headerView
    }
}

extension IKEventsViewController: UITableViewDelegate{
    
    //MARK: UITableView delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //hide picker or keyboard if necessary
        if let p = picker {
            p.dismissViewWithoutCompletion()
        }
        searchBar.resignFirstResponder()
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let lecture = dataSource[indexPath.section].lectures[indexPath.row]
        
        self.performSegue(withIdentifier: IKAppConstants.segueFromEventListToDetails, sender: lecture)
    }
    
    //MARK: UIScrollviewDelegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let status = IKAppConstants.IKEventFilterStatus(rawValue: filterButton.tag)
        if status == .open{
            filterButtonTapped(filterButton)
        }

    }
    
    //MARK: Filer event listeners
    @objc func endEditInView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        if let p = picker {
            p.dismissViewWithoutCompletion()
        }
        self.view.removeGestureRecognizer(endEditRecognizer)
    }
    
    @objc func startOrEndDateTapped(sender: UITapGestureRecognizer) {
        let tag = sender.anyTag as! IKAppConstants.IKEventFilterDateLabel
        
        if picker == nil {
            endEditInView(sender: sender)
            picker = DateTimePicker.show()
            picker!.delegate = self
        }
        
        var selectedDate = Date()
        
        switch tag {
        case .start:
            self.startDateLabel.backgroundColor = UIColor.labelEditingBG()
            self.endDateLabel.backgroundColor = UIColor.clear
            
            if (eventFilter.start != nil && eventFilter.start! > 0){
                selectedDate = Date(timeIntervalSince1970: Double(eventFilter.start!))
            }
        case .end:
            self.startDateLabel.backgroundColor = UIColor.clear
            self.endDateLabel.backgroundColor = UIColor.labelEditingBG()
            if (eventFilter.end != nil && eventFilter.end! > 0){
                selectedDate = Date(timeIntervalSince1970: Double(eventFilter.end!))
            }
        }
        if picker!.selectedDate != selectedDate {
            picker!.selectedDate = selectedDate
            picker!.resetTime()
        }
        picker!.selectedDate = selectedDate
        picker!.updateCollectionView(to: selectedDate)
        
        picker!.completionHandler = { date in
            switch tag {
            case .start:
                self.startDateLabel.text = IKDateTimeUtil.dateToString(date)
                
                let ef = self.eventFilter.copy()
                ef.start = IKDateTimeUtil.dateToTimeStamp(date)
                self.eventFilter = ef
            case .end:
                self.endDateLabel.text = IKDateTimeUtil.dateToString(date)
                
                let ef = self.eventFilter.copy()
                ef.end = IKDateTimeUtil.dateToTimeStamp(date)
                self.eventFilter = ef
            }
        }
    }
}

extension IKEventsViewController: IKLabelDelegate{
    func labelTextDidDeleted(label: IKLabel!) {
        let tag = label.anyTag as! IKAppConstants.IKEventFilterDateLabel
        switch tag {
        case .start:
            let ef = self.eventFilter.copy()
            ef.start = nil
            self.eventFilter = ef
        case .end:
            let ef = self.eventFilter.copy()
            ef.end = nil
            self.eventFilter = ef
        }
    }
}

extension IKEventsViewController: DateTimePickerDelegate{
    func dateTimePickerDidDismiss(picker: DateTimePicker) {
        self.picker = nil
        
        self.endDateLabel.backgroundColor = UIColor.clear
        self.startDateLabel.backgroundColor = UIColor.clear
    }
}

extension IKEventsViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let p = picker {
            p.dismissViewWithoutCompletion()
        }
        self.view.addGestureRecognizer(endEditRecognizer)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.removeGestureRecognizer(endEditRecognizer)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let ef = self.eventFilter.copy()
        ef.name = searchText
        self.eventFilter = ef
    }
}

extension IKEventsViewController: IKCheckBoxDelegate{
    func chechboxStateChanged(checked: Bool){
        let ef = self.eventFilter.copy()
        ef.teachersOffering = checked
        self.eventFilter = ef
        
        //hide picker or keyboard if necessary
        if let p = picker {
            p.dismissViewWithoutCompletion()
        }
        searchBar.resignFirstResponder()
    }
}
