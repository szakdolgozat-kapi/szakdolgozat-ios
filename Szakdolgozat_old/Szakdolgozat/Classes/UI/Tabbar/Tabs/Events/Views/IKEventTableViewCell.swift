//
//  IKEventTableViewCell.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 08..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKEventTableViewCell: UITableViewCell {
    
    public static let reuseIdentifier = "IKEventTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cellImageView: CachedImageView!
    
    public func configureCell(lecture: IKLecture) {
        if let url = lecture.imageUrl{
            cellImageView.emptyImage = #imageLiteral(resourceName: "ik_logo")
            cellImageView.loadImage(urlString: url)
        }else{
            cellImageView.image = #imageLiteral(resourceName: "ik_logo")
        }
        
        nameLabel.text = lecture.name
        infoLabel.text = lecture.info
        
        if (lecture.startTimeStamp != nil && lecture.startTimeStamp! > 0) {
            dateLabel.text = IKDateTimeUtil.timeStampToDateString(timeStamp: lecture.startTimeStamp)
            
            if (lecture.endTimeStamp != nil && lecture.endTimeStamp! > 0) {
                let tmp = dateLabel.text! + " - " + IKDateTimeUtil.timeStampToDateString(timeStamp: lecture.endTimeStamp)
                dateLabel.text = tmp
            }
            
        }else{
            dateLabel.text = ""
        }
    }

}
