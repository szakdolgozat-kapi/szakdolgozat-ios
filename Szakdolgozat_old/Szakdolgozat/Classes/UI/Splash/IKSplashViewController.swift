//
//  IKSplashViewController.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 05..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKSplashViewController: IKBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let eventDataManager = IKAppDependencies.sharedInstance.getEventDataManager()
        eventDataManager.getAllEvent { (events, error) in
            var downloadedEvents = Array<IKEvent>()
            if let e = events{
                downloadedEvents = e
            }
            
            self.performSegue(withIdentifier: IKAppConstants.segueFromSplashToMain, sender: downloadedEvents)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == IKAppConstants.segueFromSplashToMain && sender is Array<IKEvent>) {
            let destinationVC = segue.destination as! IKTabbarViewController
            destinationVC.initialEvents = sender as! Array<IKEvent>
        }
        super.prepare(for: segue, sender: sender)
    }
}
