//
//  QuestionDataManagerInterface.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol IKQuestionDataManagerInterface{
    
    func getLecture(id: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ()))
    func canIAsk(lectureId: String, closure :@escaping ((_ canAsk: Bool?, _ lecture: IKLecture?, _ error: NSError?) -> ()))
    func askAQuestion(question: IKQuestion, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ()))
    func startLecture(identifier: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ()))
    func getQuestions(lectureId: String, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ()))
    func getNewQuestions(lectureId: String, lastQuestionId: String?, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ()))
    func acceptQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ()))
    func declineQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ()))
}
