//
//  IKQuestionDataManager.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit
import Alamofire

class IKQuestionDataManager: IKQuestionDataManagerInterface {

    public static let sharedInstance = IKQuestionDataManager()
    private let baseUrl: String = IKConfiguration.sharedInstance.getStringForKey(key: IKAppConstants.configApiURL)
    let defaultHeaders = ["UUID" : UUID().uuidString]
    
    func getLecture(id: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getLecture
        let params = ["lectureId":id]
        
        Alamofire.request(url, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let dict = dataDict["lecturePersistence"] as? Dictionary<String, Any>{
                closure(IKLecture(dict: dict), nil)
                return
            }
            
            let error = NSError.create(status: IKStatus.defaultError())
            closure(nil, error)
        }
    }
    
    func canIAsk(lectureId: String, closure :@escaping ((_ canAsk: Bool?, _ lecture: IKLecture?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getCanIAsk
        let params = ["lectureId":lectureId]
        
        Alamofire.request(url, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(false, nil, error)
                return
            }
            
            var lecture:IKLecture?
            var canAsk:Bool = false
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let dict = dataDict["lecturePersistence"] as? Dictionary<String, Any>{
                lecture = IKLecture(dict: dict)
            }
            if let b = dataDict["boolean"] as? Bool{
                canAsk = b
            }
            
            closure(canAsk, lecture, nil)
        }
    }
    
    func askAQuestion(question: IKQuestion, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.postAsk
        let params = ["lectureId":question.lectureId!,
                      "question":question.questionString!]
        
        Alamofire.request(url, method: .post, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(false, error)
                return
            }
            
            var success:Bool = false
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let b = dataDict["boolean"] as? Bool{
                success = b
            }
            
            closure(success, nil)
        }
    }
    
    func startLecture(identifier: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.postStartLecture
        let params = ["teacherCode":identifier]
        
        Alamofire.request(url, method: .post, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let dict = dataDict["lecturePersistence"] as? Dictionary<String, Any>{
                closure(IKLecture(dict: dict), nil)
                return
            }
            
            let error = NSError.create(status: IKStatus.defaultError())
            closure(nil, error)
        }
    }
    
    func getQuestions(lectureId: String, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getQuestions
        let params = ["lectureId":lectureId]
        
        Alamofire.request(url, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let array = dataDict["treeSet"] as? Array<Any>{
                closure(IKQuestion.questionArray(array: array), nil)
                return
            }
            
            let error = NSError.create(status: IKStatus.defaultError())
            closure(nil, error)
        }
    }
    
    func getNewQuestions(lectureId: String, lastQuestionId: String?, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getNewQuestions
        let params = ["lectureId":lectureId,
                      "lastQuestionId":lastQuestionId != nil ? lastQuestionId! : ""]
        
        Alamofire.request(url, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let array = dataDict["arrayList"] as? Array<Any>{
                closure(IKQuestion.questionArray(array: array), nil)
                return
            }
            
            let error = NSError.create(status: IKStatus.defaultError())
            closure(nil, error)
        }
    }
    
    func acceptQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.postAcceptQuestion
        let params = ["lectureId":lectureId,
                      "questionId":questionId]
        
        Alamofire.request(url, method: .post, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(false, error)
                return
            }
            
            var success:Bool = false
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let b = dataDict["boolean"] as? Bool{
                success = b
            }
            
            closure(success, nil)
        }
    }
    
    func declineQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.postDeclineQuestion
        let params = ["lectureId":lectureId,
                      "questionId":questionId]
        
        Alamofire.request(url, method: .post, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(false, error)
                return
            }
            
            var success:Bool = false
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let b = dataDict["boolean"] as? Bool{
                success = b
            }
            
            closure(success, nil)
        }
    }
    
}
