//
//  IKQuestionDataManagerMock.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKQuestionDataManagerMock: IKQuestionDataManagerInterface {

    public static let sharedInstance = IKQuestionDataManagerMock()
    
    
    func getLecture(id: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKQuestionDataManagerMock.fakeLecture(), nil)
        }
    }
    
    func canIAsk(lectureId: String, closure :@escaping ((_ canAsk: Bool?, _ lecture: IKLecture?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(false, IKQuestionDataManagerMock.fakeLecture(), nil)
        }
    }
    
    func askAQuestion(question: IKQuestion, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(true, nil)
        }
    }
    
    func startLecture(identifier: String, closure :@escaping ((_ lecture: IKLecture?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKQuestionDataManagerMock.fakeLecture(), nil)
        }
    }
    
    func getQuestions(lectureId: String, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKQuestionDataManagerMock.fakeQuestions(), nil)
        }
    }
    
    func getNewQuestions(lectureId: String, lastQuestionId: String?, closure :@escaping ((_ lecture: Array<IKQuestion>?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKQuestionDataManagerMock.fakeQuestions(), nil)
        }
    }
    
    func acceptQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(true, nil)
        }
    }
    
    func declineQuestion(lectureId: String, questionId: String, closure :@escaping ((_ success: Bool?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(true, nil)
        }
    }
        
    //MARK generate mock data
    
    private static func fakeLecture() -> IKLecture {
        let lecture = IKLecture()
        
        lecture.imageUrl = "https://fanart.tv/fanart/music/ddeb3502-8693-4619-b41d-263105f84477/musiclogo/example-4e0660c8549ac.png"
        
        let counter:Int = 1
        
        lecture.name = "Lorem ipsum dolor sit amet: "
        lecture.info = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        lecture.startTimeStamp = 1484347957 + (counter * 3600)
        lecture.endTimeStamp = lecture.startTimeStamp! + 5400
        
        let o0 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -0", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        let o1 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -1", course: "course", offer: "offer")
        let o2 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -2", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
        let o3 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -3", course: "course", offer: "offer")
        
        lecture.teachersOffering = [o0, o1, o2, o3]
        
        lecture.eventName = "Teszt event"
        lecture.placeName = "IK-F01"
        lecture.askAQuestionIdentifier = "0001"
        
        return lecture
    }
    
    private static func fakeQuestions() -> Array<IKQuestion> {
        var questionArray = Array<IKQuestion>()
        let q0 = IKQuestion()
        q0.questionString = "ausdfoinasdnf 0"
        q0.identifier = "0"
        q0.lectureId = "0001"
        questionArray.append(q0)
        
        let q1 = IKQuestion()
        q1.questionString = "ausdfoinasdnf 1"
        q1.identifier = "1"
        q1.lectureId = "0001"
        questionArray.append(q1)
        
        let q2 = IKQuestion()
        q2.questionString = "ausdfoinasdnf 2"
        q2.identifier = "2"
        q2.lectureId = "0001"
        questionArray.append(q2)
        
        return questionArray
    }
    
}
