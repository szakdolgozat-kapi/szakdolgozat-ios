//
//  NetworkManager.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager: NetworkManagerProtocol {
    public static let sharedInstance = NetworkManager()

    private struct PrivateConstants {
        public static let uuid: String = IKAppConstants.appPrefix.appending("UUID")
    }

    private static var uuid: String {
        if let uuid = UserDefaults.standard.string(forKey: PrivateConstants.uuid) {
            return uuid
        }
        let uuid = UUID().uuidString
        UserDefaults.standard.set(uuid, forKey: PrivateConstants.uuid)
        UserDefaults.standard.synchronize()
        return uuid
    }

    private static func defaultHeaders() -> HTTPHeaders {
        return ["UUID" : uuid]
    }

    private static func appendDefaultHeaders(to headers: HTTPHeaders?) -> HTTPHeaders {
        guard let headers = headers else {
            return self.defaultHeaders()
        }

        return headers.merging(self.defaultHeaders()) { (first, last) -> String in
            first
        }
    }

    func requestObject<T: Codable>(request: IKRequestProtocol,
                                   type: T.Type,
                                   success: ((T) -> Void)? = nil,
                                   failure: ((NSError) -> Void)? = nil,
                                   cancel: (() -> Void)? = nil) {

        let headers = NetworkManager.appendDefaultHeaders(to: request.headers)
        
        let request = Alamofire.request(request.url,
                                        method: request.method,
                                        parameters: request.parameters,
                                        encoding: request.encoding,
                                        headers: headers)

        request.responseJSON { (response) in
            guard response.result.isSuccess else {
                failure?(NSError.networkError())
                debugPrint(response.result.error ?? "")
                return
            }

            guard let data = response.data else {
                failure?(NSError.responseDataIsNil())
                return
            }

            var name = String(describing: T.self)
            name = name.uncapitalizingFirstLetter()

            do {
                let decodedJson = try JSONDecoder().decode(IKResponse<T>.self, from: data)
                if let payload = decodedJson.data,
                    let returnValue = payload[name],
                    let success = success {
                    success(returnValue)
                }
            } catch let error {
                debugPrint(error)
                failure?(NSError.cannotParseResponse())
            }
        }
    }
}
