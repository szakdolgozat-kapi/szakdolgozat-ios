//
//  NetworkManagerProtocol.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol NetworkManagerProtocol {
    func requestObject<T: Codable>(request: IKRequestProtocol,
                                   type: T.Type,
                                   success: ((T) -> Void)?,
                                   failure: ((NSError) -> Void)?,
                                   cancel: (() -> Void)?)
}

// NOTE: - Protocols cannot contains default values, but protocol extensions do
extension NetworkManagerProtocol {
    func requestObject<T: Codable>(request: IKRequestProtocol,
                                   type: T.Type,
                                   success: ((T) -> Void)? = nil,
                                   failure: ((NSError) -> Void)? = nil,
                                   cancel: (() -> Void)? = nil) {
        requestObject(request: request,
                      type: type,
                      success: success,
                      failure: failure,
                      cancel: cancel)
    }
}
