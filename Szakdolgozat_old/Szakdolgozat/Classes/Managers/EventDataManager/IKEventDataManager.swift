
//
//  IKEventDataManager.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit
import Alamofire

class IKEventDataManager: IKEvenetDataManagerInterface {
    
    public static let sharedInstance = IKEventDataManager()

    private let baseUrl: String = IKConfiguration.sharedInstance.getStringForKey(key: IKAppConstants.configApiURL)
    private let networkManager = IKAppDependencies.sharedInstance.getNetworkManager()

    // TODO: - Delete this after refact
    private let defaultHeaders = ["UUID" : UUID().uuidString]
    
    func getAllEvent(closure :@escaping ((_ array: Array<IKEvent>?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getEvents
        
        Alamofire.request(url, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            let eventArray = IKJSONProcessor.parseEvents(data: response.data)
            closure(eventArray, nil)
        }

        // TODO: - Use something like this:
        // TODO: - delete this example
        /*let asdRequest = AsdRequest(baseUrl: /*baseUrl*/"http://127.0.0.1:8080/", parameters: ["name": "name"])
        networkManager.requestObject(request: asdRequest,
                                            type: Asd.self,
                                            success: { (response) in
                                                debugPrint(response.description)
        }, failure: { (error) in
            debugPrint(error)
        })*/

        /*let asdListRequest = AsdListRequest(baseUrl: "http://127.0.0.1:8080/")
        networkManager.requestObject(request: asdListRequest,
                                     type: AsdResponse.self,
                                     success: { (response) in
                                        debugPrint(response.description)
        }, failure: { (error) in
            debugPrint(error)
        })*/
    }
    
    func getEvent(id: String, closure :@escaping ((_ event: IKEvent?, _ error: NSError?) -> ())){
        let url = baseUrl + IKAppConstants.getEvent
        let params = ["eventId":id]
        
        Alamofire.request(url, parameters: params, headers: defaultHeaders).responseJSON { (response) in
            let status = IKJSONProcessor.parseStatus(data: response.data)
            guard status.status == 200 else{
                let error = NSError.create(status: status)
                closure(nil, error)
                return
            }
            
            
            let dataDict = IKJSONProcessor.parseDataDictionary(data: response.data)
            if let dict = dataDict["eventPersistence"] as? Dictionary<String, Any>{
                closure(IKEvent(dict: dict), nil)
                return
            }
            
            let error = NSError.create(status: IKStatus.defaultError())
            closure(nil, error)
        }
    }
}
