//
//  EvenetDataManagerInterface.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import Foundation

protocol IKEvenetDataManagerInterface {
    
    func getAllEvent(closure :@escaping ((_ array: Array<IKEvent>?, _ error: NSError?) -> ()))
    func getEvent(id: String, closure :@escaping ((_ event: IKEvent?, _ error: NSError?) -> ()))
    
}
