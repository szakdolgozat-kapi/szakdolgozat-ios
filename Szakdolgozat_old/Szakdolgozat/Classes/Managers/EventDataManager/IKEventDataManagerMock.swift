//
//  IKEventDataManagerMock.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKEventDataManagerMock: IKEvenetDataManagerInterface {

    public static let sharedInstance = IKEventDataManagerMock()
    
    func getAllEvent(closure :@escaping ((_ array: Array<IKEvent>?, _ error: NSError?) -> ())){
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKEventDataManagerMock.mockEventList(), nil)
        }
    }
    
    func getEvent(id: String, closure :@escaping ((_ event: IKEvent?, _ error: NSError?) -> ())){
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            closure(IKEvent(), nil)
        }
    }
    
    //MARK generate mock data
    
    private static func mockEventList() -> Array<IKEvent> {
        var array = Array<IKEvent>()
        var counter: Int = 0

        for i in 0...2{
            let event = IKEvent()
            event.name = "Event " + String(i)
            event.lectures = Array()
            
            for j in 0...4{
                let lecture = IKLecture()
                
                switch j {
                case 0:
                    lecture.imageUrl = nil
                case 1:
                    lecture.imageUrl = "https://thumb1.shutterstock.com/display_pic_with_logo/3016328/303212120/stock-vector-example-free-grunge-retro-blue-isolated-stamp-303212120.jpg"
                case 2:
                    lecture.imageUrl = "http://s3.amazonaws.com/libapps/accounts/27060/images/example.png"
                case 3:
                    lecture.imageUrl = nil
                case 4:
                    lecture.imageUrl = "https://fanart.tv/fanart/music/ddeb3502-8693-4619-b41d-263105f84477/musiclogo/example-4e0660c8549ac.png"
                case 5:
                    lecture.imageUrl = "http://image.shutterstock.com/z/stock-photo-sample-stamp-showing-example-symbol-or-taste-104401619.jpg"
                default:
                    lecture.imageUrl = nil
                }
                
                
                lecture.name = "Lorem ipsum dolor sit amet: " + String(i) + "-" + String(j)
                lecture.info = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                lecture.startTimeStamp = 1484347957 + (counter * 3600)
                lecture.endTimeStamp = lecture.startTimeStamp! + 5400
                
                let o0 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -0", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                let o1 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -1", course: "course", offer: "offer")
                let o2 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -2", course: "course", offer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                let o3 = IKTeachersOffering(teacher: "teacher " + String(counter) + " -3", course: "course", offer: "offer")
                
                lecture.teachersOffering = [o0, o1, o2, o3]
                
                lecture.eventName = event.name
                lecture.placeName = "IK-F01"
                lecture.askAQuestionIdentifier = counter % 2 == 0 ? nil : "000" + String(counter)
                
                counter += 1
                event.lectures!.append(lecture)
            }
            
            array.append(event)
        }

        return array
    }
    
}
