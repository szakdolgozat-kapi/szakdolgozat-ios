//
//  IKConfiguration.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKConfiguration {
    
    public static let sharedInstance = IKConfiguration()
    private var env:IKAppConstants.IKEnvironment = IKAppConstants.IKEnvironment.unknown
    private var configuration: Dictionary<String,Any>? = nil
    
    public func setEnv(env: IKAppConstants.IKEnvironment){
        guard env != IKAppConstants.IKEnvironment.unknown else {
            assertionFailure("Unknown environment! Please set a valid environment!")
            return
        }
        
        self.env = env
        
        let filePath = Bundle.main.bundlePath + "/Configuration.plist"
        let cfg = NSDictionary(contentsOfFile: filePath)
        self.configuration = cfg as? Dictionary<String,Any>
        
        if (self.configuration == nil) {
            assertionFailure("Configuration.plist does't exists!")
        } else if (self.configuration!.count == 0){
            assertionFailure("Configuration.plist does't contains any key value pair!")
        }
    }
    
    public func configValueForKey(_ key: String) -> Any?{
        guard self.env != IKAppConstants.IKEnvironment.unknown else {
            assertionFailure("Unknown environment! Please set a valid environment!")
            return nil
        }
        
        if let dict = configuration![key] as? Dictionary<String, Any>{
            return dict[self.env.rawValue]
        }else if let any = configuration![key]{
            return any
        }else{
            assertionFailure("Configuration.plist does't contains " + key)
            return nil
        }
    }

    public func getBooleanForKey(key: String) -> Bool{
        if let config = configValueForKey(key) as? Bool{
            return config
        }else{
            assertionFailure("This config is not a boolean, for key: " + key)
            return false
        }
    }

    public func getStringForKey(key: String) -> String{
        if let config = configValueForKey(key) as? String{
            return config
        }else{
            assertionFailure("This config is not a string, for key: " + key)
            return ""
        }
    }
    
    public func getIntForKey(key: String) -> Int{
        if let config = configValueForKey(key) as? Int{
            return config
        }else{
            assertionFailure("This config is not an Int, for key: " + key)
            return 0
        }
    }
    
    public func getFloatForKey(key: String) -> Float{
        if let config = configValueForKey(key) as? Float{
            return config
        }else{
            assertionFailure("This config is not a Float, for key: " + key)
            return 0
        }
    }
    
    public func getDoubleForKey(key: String) -> Double{
        if let config = configValueForKey(key) as? Double{
            return config
        }else{
            assertionFailure("This config is not a Double, for key: " + key)
            return 0
        }
    }
    
    public func getArrayForKey(key: String) -> Array<Any>{
        if let config = configValueForKey(key) as? Array<Any>{
            return config
        }else{
            assertionFailure("This config is not an Array, for key: " + key)
            return Array()
        }
    }
    
    public func getDictionaryForKey(key: String) -> Dictionary<String, Any>{
        if let config = configValueForKey(key) as? Dictionary<String, Any>{
            return config
        }else{
            assertionFailure("This config is not a Dictionary, for key: " + key)
            return Dictionary()
        }
    }
}
