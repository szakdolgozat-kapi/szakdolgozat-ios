//
//  String+Extensions.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 16..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import Foundation

extension String{
    func convertToDictionary() -> [String: Any] {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return Dictionary<String, Any>()
    }

    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }

    func uncapitalizingFirstLetter() -> String {
        return prefix(1).lowercased() + dropFirst()
    }

    mutating func uncapitalizeFirstLetter() {
        self = self.uncapitalizingFirstLetter()
    }
}
