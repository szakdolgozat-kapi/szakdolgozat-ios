//
//  NSObject+Extensions.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 12..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import Foundation

extension NSObject{
    fileprivate struct ObjectTag {
        static var anyTag:Any?
        static var nsobjectTag:NSObject?
    }
    
    var anyTag:Any?{
        get {
            return objc_getAssociatedObject(self, &ObjectTag.anyTag) as Any?
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &ObjectTag.anyTag, newValue as Any?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    var NSObjectTag:NSObject?{
        get {
            return objc_getAssociatedObject(self, &ObjectTag.nsobjectTag) as? NSObject
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &ObjectTag.nsobjectTag, newValue as NSObject?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}
