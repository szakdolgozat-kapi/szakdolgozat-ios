//
//  UIColor+Extensions.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 12..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

extension UIColor{
    
    public static func labelEditingBG() -> UIColor {
        return UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 0.5)
    }
    
}
