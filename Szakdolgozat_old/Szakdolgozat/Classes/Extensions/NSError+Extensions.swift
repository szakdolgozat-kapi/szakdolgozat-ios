//
//  NSError+Extensions.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 16..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import Foundation
extension NSError{
    static func create(status: IKStatus) -> NSError {
        var message:String
        let local = NSLocalizedString("locale_code", comment: "")
        if local == "hu_HU" {
            message = status.messageHu!
        } else {
            message = status.messageEn!
        }
        
        return NSError(domain: message, code: status.status!, userInfo: nil)
    }
}

extension NSError {
    // TODO: - Localize
    public class func cannotParseResponse() -> NSError {
        let info = [NSLocalizedDescriptionKey: "Can't parse response. Please report a bug."]
        return NSError(domain: String(describing: self), code: 0, userInfo: info)
    }

    public class func responseDataIsNil() -> NSError {
        let info = [NSLocalizedDescriptionKey: "Response data is nil. Please report a bug."]
        return NSError(domain: String(describing: self), code: 0, userInfo: info)
    }

    public class func networkError() -> NSError {
        let info = [NSLocalizedDescriptionKey: "Not connected to internet."]
        return NSError(domain: String(describing: self), code: 503, userInfo: info)
    }
}
