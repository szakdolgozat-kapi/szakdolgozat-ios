//
//  AsdRequest.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation
import Alamofire

struct AsdRequest: IKRequestProtocol {
    var baseUrl: String

    var endpoint: String {
        return "asd2"
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters?

    var encoding: ParameterEncoding {
        return URLEncoding.default
    }

    var headers: HTTPHeaders?

    init(baseUrl: String, parameters: [String: Any]? = nil, headers: [String: String]? = nil) {
        self.baseUrl = baseUrl
        self.parameters = parameters
        self.headers = headers
    }
}
