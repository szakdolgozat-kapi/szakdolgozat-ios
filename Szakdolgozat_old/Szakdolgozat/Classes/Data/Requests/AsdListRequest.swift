//
//  AsdListRequest.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation
import Alamofire

struct AsdListRequest: IKRequestProtocol {
    var baseUrl: String

    var endpoint: String {
        return "asdList"
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: Parameters?

    var encoding: ParameterEncoding {
        return URLEncoding.default
    }

    var headers: HTTPHeaders?

    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
}

