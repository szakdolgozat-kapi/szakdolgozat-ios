//
//  IKResponseProtocol.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation
import Alamofire

protocol IKRequestProtocol {
    var url: URLConvertible { get }
    var baseUrl: String { get }
    var endpoint: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var headers: HTTPHeaders? { get }
}

extension IKRequestProtocol {
    var url: URLConvertible {
        return baseUrl.appending(endpoint)
    }
}
