//
//  IKEvent.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 07..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKEvent: Hashable {
    var identifier: String!
    var name: String?
    var info: String?
    var startTimeStamp: Int?
    var endTimeStamp: Int?
    
    var lectures: Array<IKLecture>!

    var hashValue: Int{
        get{
            let n = (name != nil) ? name!.hashValue : 0
            let e = (endTimeStamp != nil) ? endTimeStamp!.hashValue : 0
            return n + e
        }
    }
    
    static func ==(lhs: IKEvent, rhs: IKEvent) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func copy() -> IKEvent {
        return IKEvent(event: self)
    }
    
    init(event: IKEvent) {
        lectures = Array(event.lectures)
        
        name = event.name
        info = event.info
        startTimeStamp = event.startTimeStamp
        endTimeStamp = event.endTimeStamp
    }
    
    required init() {
    }
    
    init(dict: Dictionary<String, Any>?) {
        guard dict != nil && dict!.count > 0 else {
            return
        }
        
        identifier = String(dict!["identifier"] as! Int)
        name = dict!["name"] as? String
        info = dict!["info"] as? String
        startTimeStamp = dict!["startTimeStamp"] as? Int
        endTimeStamp = dict!["endTimeStamp"] as? Int
        lectures = IKLecture.lectureArray(array: dict!["lectures"] as? Array<Any>)
    }
    
    static func eventArray(array: Array<Any>?) -> Array<IKEvent> {
        var result = Array<IKEvent>()
        guard array != nil && array!.count > 0 else {
            return result
        }
        
        if let a = array as? Array<Dictionary<String, Any>>{
            a.forEach({ (dict) in
                result.append(IKEvent(dict: dict))
            })
        }
        return result
    }
}
