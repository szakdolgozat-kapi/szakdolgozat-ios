//
//  IKEventFilter.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 09..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKEventFilter {
    var name: String?
    var start: Int?
    var end: Int?
    var teachersOffering: Bool?
    
    func copy() -> IKEventFilter{
        return IKEventFilter(eventFilter: self)
    }
    
    init(eventFilter: IKEventFilter) {
        name = eventFilter.name
        start = eventFilter.start
        end = eventFilter.end
    }
    
    required init() {
        
    }
    
    var description: String {
        //return "MyClass \(string)"
        return "IKEventFilter - name: \(self.name), start: \(self.start), end: \(self.end)"
    }
}
