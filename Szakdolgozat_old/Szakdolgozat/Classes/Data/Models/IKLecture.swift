//
//  IKLecture.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 13..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKLecture {
    var identifier: String!
    var name: String?
    var info: String?
    var eventName: String?
    var placeName: String?
    var startTimeStamp: Int?
    var endTimeStamp: Int?
    var teachersOffering: Array<IKTeachersOffering>?
    var imageUrl: String?
    var askAQuestionIdentifier: String?
    
    init() {
    }
    
    init(dict: Dictionary<String, Any>?) {
        guard dict != nil && dict!.count > 0 else {
            return
        }
        
        identifier = String(dict!["identifier"] as! Int)
        name = dict!["name"] as? String
        eventName = dict!["eventName"] as? String
        placeName = dict!["placeName"] as? String
        imageUrl = dict!["imageUrl"] as? String
        askAQuestionIdentifier = dict!["askAQuestionIdentifier"] as? String
        info = dict!["info"] as? String
        startTimeStamp = dict!["startTimeStamp"] as? Int
        endTimeStamp = dict!["endTimeStamp"] as? Int
        teachersOffering = IKTeachersOffering.offeringArray(array: dict!["offers"] as? Array)
    }
    
    static func lectureArray(array: Array<Any>?) -> Array<IKLecture> {
        var result = Array<IKLecture>()
        guard array != nil && array!.count > 0 else {
            return result
        }
        
        if let a = array as? Array<Dictionary<String, Any>>{
            a.forEach({ (dict) in
                result.append(IKLecture(dict: dict))
            })
        }
        return result.sorted(by: { (l1, l2) -> Bool in
            l1.startTimeStamp! < l2.startTimeStamp!
        })
    }
    
    
}
