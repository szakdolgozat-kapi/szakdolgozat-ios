//
//  IKQuestion.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKQuestion {
    var identifier: String?
    var questionString: String?
    var lectureId: String!
    
    init() {
    }
    
    init(dict: Dictionary<String, Any>?) {
        guard dict != nil && dict!.count > 0 else {
            return
        }
        
        identifier = dict!["id"] as? String
        questionString = dict!["questionString"] as? String
        lectureId = dict!["lectureId"] as? String
    }
    
    static func questionArray(array: Array<Any>?) -> Array<IKQuestion> {
        var result = Array<IKQuestion>()
        guard array != nil && array!.count > 0 else {
            return result
        }
        
        if let a = array as? Array<Dictionary<String, Any>>{
            a.forEach({ (dict) in
                result.append(IKQuestion(dict: dict))
            })
        }
        return result.sorted(by: { (q1, q2) -> Bool in
            q1.identifier! < q2.identifier!
        })
    }
}
