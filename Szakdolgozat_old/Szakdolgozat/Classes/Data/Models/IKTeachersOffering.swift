//
//  IKTeachersOffering.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 14..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKTeachersOffering {
    var teacher: String?
    var course: String?
    var offer: String?
    
    init(teacher: String? = nil, course: String? = nil, offer: String? = nil) {
        self.teacher = teacher
        self.course = course
        self.offer = offer
    }
    
    init(dict: Dictionary<String, Any>?) {
        guard dict != nil && dict!.count > 0 else {
            return
        }
        teacher = dict!["teacher"] as? String
        course = dict!["course"] as? String
        offer = dict!["offer"] as? String
    }
    
    static func offeringArray(array: Array<Any>?) -> Array<IKTeachersOffering> {
        var result = Array<IKTeachersOffering>()
        guard array != nil && array!.count > 0 else {
            return result
        }
        
        if let a = array as? Array<Dictionary<String, Any>>{
            a.forEach({ (dict) in
                result.append(IKTeachersOffering(dict: dict))
            })
        }
        return result.sorted(by: { (o1, o2) -> Bool in
            o1.teacher! < o2.teacher!
        })
    }
}
