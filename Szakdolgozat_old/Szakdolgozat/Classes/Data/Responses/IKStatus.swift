//
//  IKStatus.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 16..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKStatus: Codable {
    var status: Int?
    var messageEn: String?
    var messageHu: String?
    
    init() {
        
    }
    
    init(dict: Dictionary<String, Any>) {
        status = dict["status"] as? Int
        messageEn = dict["messageEn"] as? String
        messageHu = dict["messageHu"] as? String
    }
    
    static func defaultError() -> IKStatus {
        let s = IKStatus()
        s.status = 500
        s.messageEn = NSLocalizedString("default.error.message", comment: "")
        s.messageHu = NSLocalizedString("default.error.message", comment: "")
        return s
    }
}
