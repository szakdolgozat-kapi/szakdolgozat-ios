//
//  AsdResponse.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation

// TODO: - delete this:
class AsdResponse: Codable {
    let asdList: [Asd]?

    var description: String {
        guard let asdList = asdList else {
            return "empty asdList"
        }

        var desc = ""
        for asd in asdList {
            desc.append(asd.description)
        }
        return desc
    }
}

