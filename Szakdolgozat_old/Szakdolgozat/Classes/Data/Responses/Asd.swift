//
//  Asd.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2018. 08. 26..
//  Copyright © 2018. Kapi Zoltán. All rights reserved.
//

import Foundation

// TODO: - delete this:
class Asd: Codable {
    let id: Int
    let name: String

    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }

    var description: String {
        return "Asd: {id: ".appending(id.description).appending(", name: ").appending(name).appending("}")
    }
}
