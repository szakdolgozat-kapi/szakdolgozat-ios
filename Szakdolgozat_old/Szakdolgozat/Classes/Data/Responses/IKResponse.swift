//
//  IKResponse.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 16..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKResponse<T: Codable>: Codable {
    var status: IKStatus?
    var data: [String: T]?
}
