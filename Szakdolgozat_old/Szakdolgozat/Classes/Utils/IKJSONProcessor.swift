//
//  IKJSONProcessor.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 16..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKJSONProcessor {
    
    public static func parseStatus(data: Data?) -> IKStatus {
        let status = IKStatus.defaultError()
        guard data != nil else {
            return status
        }
        
        let dataString = IKJSONProcessor.parseString(data: data)
        let dataDict:Dictionary<String, Any> = dataString.convertToDictionary()
        if let statusDict = dataDict["status"] as? Dictionary<String, Any>{
            return IKStatus(dict: statusDict)
        }
        
        return status
        
    }
    
    public static func parseEvents(data: Data?) -> Array<IKEvent> {
        let array = Array<IKEvent>()
        guard data != nil else {
            return array
        }
        
        let dataDict = IKJSONProcessor.parseDataDictionary(data: data)
        if let eventArray = dataDict["arrayList"] as? Array<Any>{
            return IKEvent.eventArray(array: eventArray)
        }
        
        
        return array.sorted(by: { (e1, e2) -> Bool in
            e1.startTimeStamp! < e2.startTimeStamp!
        })
    }
    
    public static func parseString(data: Data?) -> String {
        guard data != nil else {
            return ""
        }
        
        if let dataString = String(data: data!, encoding: .utf8){
            return dataString
        }
        
        return ""
    }
    
    public static func parseDataDictionary(data: Data?) -> Dictionary<String, Any> {
        let dict = Dictionary<String, Any>()
        guard data != nil else {
            return dict
        }
        
        let dataString = IKJSONProcessor.parseString(data: data)
        let dataDict:Dictionary<String, Any> = dataString.convertToDictionary()
        if let d = dataDict["data"] as? Dictionary<String, Any>{
            return d
        }
        
        return dict
    }
    
}
