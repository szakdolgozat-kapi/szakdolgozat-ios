//
//  IKDateTimeFormatter.swift
//  Szakdolgozat
//
//  Created by Kapi Zoltán on 2017. 01. 09..
//  Copyright © 2017. Kapi Zoltán. All rights reserved.
//

import UIKit

class IKDateTimeUtil {
    
    public static func timeStampToDateString(timeStamp: Int?) -> String {
        guard timeStamp != nil else {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: Double(timeStamp!))
        
        return dateToString(date)
    }
    
    public static func dateToString(_ date: Date?) -> String {
        guard date != nil else {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        let locale = NSLocalizedString("locale_code", comment: "locale key")
        dateFormatter.locale = Locale(identifier: locale)
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date!)
    }
    
    public static func dateToTimeStamp (_ date: Date?) -> Int? {
        guard date != nil else {
            return nil
        }
        
        return Int(date!.timeIntervalSince1970)
    }
}
